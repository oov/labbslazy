package upload

import (
	"bitbucket.org/oov/labbslazy/chunk"
	"bitbucket.org/oov/labbslazy/kvs"
	"errors"
	"io"
	"time"
)

type Upload struct {
	pool  kvs.Pool
	mount string
	chunk int
}

type Entry struct {
	Key     string
	AddDate int64
}

func (u *Upload) setWithEncode(name string, key []byte, data interface{}, updateOnly bool) error {
	var db kvs.KVS
	var err error
	if updateOnly {
		db, err = u.pool.GetExist(name)
	} else {
		db, err = u.pool.Get(name)
	}
	if err != nil {
		return err
	}

	return db.SetMsgpack(key, data)
}

func (u *Upload) getWithDecode(name string, key []byte, data interface{}) error {
	db, err := u.pool.GetExist(name)
	if err != nil {
		return err
	}

	return db.GetMsgpack(key, data)
}

func (u *Upload) Count() (int64, error) {
	var n int64

	err := u.getWithDecode(generalDBName(u.mount), countKey(), &n)
	if err != nil {
		return 0, err
	}

	return n, nil
}

func (u *Upload) setCount(count int64) error {
	err := u.setWithEncode(generalDBName(u.mount), countKey(), count, false)
	if err != nil {
		return err
	}

	return nil
}

func (u *Upload) SweepCount() (int64, error) {
	var n int64

	err := u.getWithDecode(generalDBName(u.mount), sweepCountKey(), &n)
	if err != nil {
		return 0, err
	}

	return n, nil
}

func (u *Upload) SetSweepCount(count int64) error {
	err := u.setWithEncode(generalDBName(u.mount), sweepCountKey(), count, false)
	if err != nil {
		return err
	}

	return nil
}

func (u *Upload) Sweep(key string) error {
	n := extractNumber(key)
	if n == 0 {
		return errors.New("upload: invalid key")
	}

	db, err := u.pool.GetExist(entryDBName(u.mount, n))
	if err != nil {
		return err
	}

	err = chunk.Remove(db, key)
	if err != nil {
		return err
	}

	return db.Delete(entryKey(n))
}

func (u *Upload) Reader(key string) (*chunk.ChunkReader, error) {
	n := extractNumber(key)
	if n == 0 {
		return nil, errors.New("upload: invalid key")
	}

	db, err := u.pool.GetExist(entryDBName(u.mount, n))
	if err != nil {
		return nil, err
	}

	return chunk.NewChunkReader(db, key)
}

func (u *Upload) Get(n int64) (s string, t time.Time, err error) {
	var entry Entry

	err = u.getWithDecode(entryDBName(u.mount, n), entryKey(n), &entry)
	if err != nil {
		return
	}

	return entry.Key, time.Unix(0, entry.AddDate), nil
}

func (u *Upload) Upload(reader io.Reader, mime string) (string, int64, error) {
	count, err := u.Count()
	if err != nil {
		return "", 0, err
	}

	count++

	err = u.setCount(count)
	if err != nil {
		return "", 0, err
	}

	key := uploadKey(count)
	db, err := u.pool.Get(entryDBName(u.mount, count))
	if err != nil {
		return "", 0, err
	}

	writer := chunk.NewChunkWriter(db, key, u.chunk, mime)
	size, err := io.Copy(writer, reader)
	if err != nil {
		return "", 0, err
	}

	err = writer.Batch().SetMsgpack(entryKey(count), &Entry{
		Key:     key,
		AddDate: time.Now().UnixNano(),
	})
	if err != nil {
		return "", 0, err
	}

	err = writer.Close()
	if err != nil {
		return "", 0, err
	}

	return key, size, nil
}

func New(pool kvs.Pool, mount string, chunk int) (*Upload, error) {
	u := &Upload{
		pool:  pool,
		mount: mount,
		chunk: chunk,
	}

	_, err := u.Count()
	if err != nil {
		u.setCount(0)
	}

	_, err = u.SweepCount()
	if err != nil {
		u.SetSweepCount(0)
	}

	return u, nil
}
