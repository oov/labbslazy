package upload

import (
	"math/rand"
	"strconv"
)

func Must(u *Upload, err error) *Upload {
	if err != nil {
		panic("upload.Must: " + err.Error())
	}
	return u
}

//投稿を保存するためのDB名を組み立てて返す
func entryDBName(mount string, n int64) string {
	num := strconv.FormatInt(n/10000, 36)
	ln := len(num)
	for i := 4 - ln; i > 0; i-- {
		num = "0" + num
	}
	return mount + "/l" + num
}

//投稿を保存する際のDB内のキー名を組み立てて返す
func entryKey(n int64) []byte {
	return []byte("u" + strconv.FormatInt(n, 36))
}

//このマウントポイントにおける全般的な情報を格納するためのDB名を組み立てて返す
func generalDBName(mount string) string {
	return mount + "/stat"
}

//総アップロード数を格納するためのキー名を返す
func countKey() []byte {
	return []byte("count")
}

//掃除が完了したインデックスを格納するためのキー名を返す
func sweepCountKey() []byte {
	return []byte("sweep-count")
}

//アップロードされたファイルを格納するためのキー名をランダムに組み立てて返す
func uploadKey(n int64) string {
	const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	const LENGTH = 24
	s := "u" + strconv.FormatInt(n, 36) + "."
	for i, cl := 0, len(chars); i < LENGTH; i++ {
		s += string(chars[rand.Intn(cl)])
	}
	return s
}

func extractNumber(uploadKey string) int64 {
	if len(uploadKey) < 3 || uploadKey[0] != 'u' {
		return 0
	}

	dotPos := -1
	for i := 0; i < len(uploadKey); i++ {
		if uploadKey[i] == '.' {
			dotPos = i
			break
		}
	}
	if dotPos == -1 {
		return 0
	}

	r, err := strconv.ParseInt(uploadKey[1:dotPos], 36, 64)
	if err != nil {
		return 0
	}

	return r
}
