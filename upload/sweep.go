package upload

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"errors"
	"github.com/golang/glog"
	"time"
)

var (
	errTooEarlyToDelete = errors.New("upload: too early to delete")
)

func (u *Upload) sweepOldOne(n int64, deadline time.Time) error {
	key, adddate, err := u.Get(n)
	if err == kvs.ErrKVSKeyNotFound {
		return nil
	}
	if err != nil {
		return err
	}

	if adddate.After(deadline) {
		return errTooEarlyToDelete
	}

	return u.Sweep(key)
}

func (u *Upload) SweepOldFiles(limit time.Duration) error {
	deadline := time.Now().Add(-limit)

	c, err := u.Count()
	if err != nil {
		glog.Errorln(err)
		return err
	}

	sc, err := u.SweepCount()
	if err != nil {
		glog.Errorln(err)
		return err
	}

	for sc < c {
		sc++

		err = u.sweepOldOne(sc, deadline)
		if err == errTooEarlyToDelete {
			break
		}

		err = u.SetSweepCount(sc)
		if err != nil {
			glog.Errorln(err)
			return err
		}

		glog.Infof("No.%d sweeped.", sc)
	}

	return nil
}
