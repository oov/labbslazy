function formatReadableFilesize(sz){
  var suffix = [ "Bytes", "KB", "MB", "GB", "TB" ], i = 0, n = 0;
  while (sz > 1000) {
    ++i;
    sz /= 1024;
  }
  if (i && sz < 100) {
    ++n;
  }
  if (i && sz < 10) {
    ++n;
  }
  return sz.toFixed(n) + " " + suffix[i];
}

function formatDate(d){
  var s = "";
  s+=d.getFullYear();
  s+="-";
  s+=d.getMonth()+1 < 10 ? "0" : "";
  s+=d.getMonth()+1;
  s+="-";
  s+=d.getDate() < 10 ? "0" : "";
  s+=d.getDate();
  s+=" ";
  s+=d.getHours() < 10 ? "0" : "";
  s+=d.getHours();
  s+=":";
  s+=d.getMinutes() < 10 ? "0" : "";
  s+=d.getMinutes();
  s+=":";
  s+=d.getSeconds() < 10 ? "0" : "";
  s+=d.getSeconds();
  s+=".";
  s+=d.getMilliseconds() < 100 ? "0" : "";
  s+=d.getMilliseconds() < 10 ? "0" : "";
  s+=d.getMilliseconds();
  return s;
}
