(function($){

$.fn.upload = function(options){
  options = $.extend({
    success: $.noop,
    error: $.noop,
    uploading: $.noop,
    inputStyle: "position:absolute;left:-1px;top:0;width:1px;height:1px",
    labelStyle: "position:absolute;display:block;top:0;left:0;width:100%;height:100%;opacity:0;filter:alpha(opacity=0);background-color:#dddddd;overflow:hidden",
    containerRelative: true
  }, options);

  function init(container){
    var tmpid = (new Date().getTime().toString(16)) + Math.floor(Math.random()*0xffffffff).toString(16);
    var labelWithInput = $('<label for="file-'+tmpid+'"><input id="file-'+tmpid+'" type="file"></label>').attr({
      'data-action': container.getAttribute('data-action'),
      'data-tmpid': tmpid
    }).on('change', function(){
      if (window.FormData){
        uploadViaAjax(labelWithInput);
      } else {
        uploadViaIframe(labelWithInput);
      }
      init(container);
    });
    if (options.labelStyle){
      labelWithInput.attr('style', options.labelStyle);
    }
    if (options.inputStyle){
      labelWithInput.find('input[type=file]').attr('style', options.inputStyle);
    }
    $(container)
      .append(labelWithInput);
  }

  function extractFilename(labelWithInput){
    var value = labelWithInput.find('input[type=file]').val();
    var r = value.match(/[\/\\]([^\/\\]+)$/i);
    if (r) return r[1];
    return value;
  }

  function uploadViaAjax(labelWithInput){
    var inputFile = labelWithInput.find('input[type=file]').get(0);
    var tmpid = labelWithInput.data('tmpid');
    var filename = extractFilename(labelWithInput);
    var xhr = new XMLHttpRequest();
    xhr.open('POST', labelWithInput.data('action'));
    xhr.setRequestHeader("X-Requested-With","XMLHttpRequest");

    xhr.onload = function(e){
      if (xhr.status == 200){
        parseResponse(tmpid, filename, xhr.responseText);
      } else {
        options.error(tmpid, filename, {Error: "内部エラーが発生しました", xhr: xhr});
      }
    };

    var formData = new FormData();
    formData.append('file', inputFile.files[0]);
    formData.append('tmpid', tmpid);
    xhr.send(formData);
    labelWithInput.remove();
    options.uploading(tmpid, filename);
  }
  
  function uploadViaIframe(labelWithInput){
    var inputFile = labelWithInput.find('input[type=file]');
    var tmpid = labelWithInput.data('tmpid');
    var filename = extractFilename(labelWithInput);
    var form = $('<form target="upld-'+tmpid+'"></form>').attr({
      action: labelWithInput.data('action'),
      method:"POST",
      enctype: "multipart/form-data",
      style: "display:none"
    }).appendTo(document.body);
    var iframe = $('<iframe id="upld-'+tmpid+'" name="upld-'+tmpid+'" src="about:blank"></iframe>').appendTo(form);
    inputFile.attr('name', 'file');
    form.append(labelWithInput);
    form.append('<input type="hidden" name="id" value="'+tmpid+'">');
    setTimeout(function(){
      iframe.on('load', function(){
        parseResponse(tmpid, filename, $('pre', iframe.get(0).contentWindow.document).text());
        setTimeout(function(){
          form.remove();
        }, 0);
      });
      iframe.on('error', function(){
        options.error(tmpid, filename, {Error: "内部エラーが発生しました", detail: $(iframe.get(0).contentWindow.document).html()});
        setTimeout(function(){
          form.remove();
        }, 0);
      });
      form.submit();
      options.uploading(tmpid, filename);
    }, 0);
  }

  function parseResponse(tmpid, filename, text){
    try{
      var json = $.parseJSON(text);
      if (json.Error){
        options.error(tmpid, filename, json);
      } else {
        options.success(tmpid, filename, json);
      }
    } catch(e) {
      options.error(tmpid, filename, {Error: "内部エラーが発生しました", detail: text});
    }
  }

  this.each(function(){
    var $this = $(this);
    //static では意図した通りには動かないので relative に変更する
    if (options.containerRelative && $this.css('position') == 'static'){
      $this.css('position', 'relative');
    }
    init(this);
  });
};

})(jQuery);
