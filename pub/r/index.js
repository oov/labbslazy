window.onunload = function(){}; //戻るのキャッシュを無効化

var lastMessage;
var uploading = 0, posting = 0;
$(function(){
  var messageForm = $('form#message-form');
  var messageFormBody = messageForm.find('textarea[name=Body]');

  $("#messages").children().each(function(){
    processLogPost($(this));
  });
  updateMore();

  messageForm.on('submit', function(){
    if ($.trim(this.Body.value) == '') return false;
    if (this.Body.value == lastMessage){
      //多重投稿
      this.Body.value = '';
      return false;
    }
    if (uploading){
      alert("まだアップロード中のファイルがあるため投稿できません。");
      return false;
    }
    var $this = $(this);
    var that = this;
    $.post(messageForm.attr('action'), $this.serializeArray()).done(function(){
      formLock(--posting);
      messageFormBody.val('').trigger('autosize.resize');
      $('#upload-list').empty();
    }).fail(function(e){
      formLock(--posting);
      alert("投稿に失敗しました。\n\n"+$.trim(e.responseText));
    });
    formLock(++posting);

    lastMessage = this.Body.value;
    return false;
  });
  messageFormBody.on('keydown', function(e){
    if (e.keyCode == 13 &&  e.shiftKey){
      $('form#message-form').submit();
      return false;
    }
  }).autosize();


  var uploadTemplate = $('#upload-template').html();
  var uploadList = $('#upload-list');
  $('#legacy-upload').remove();
  $('#upload-container').show().upload({
    uploading: function(id, name){
      ++uploading;
      var entry = $(uploadTemplate).addClass('uploading').attr('id', 'upload'+id).appendTo(uploadList);
      entry.find('.input-filename').attr({
        "id": 'upload'+id+'-filename',
        "value": name.replace(/\.\w+$/, '')
      });
      entry.find('.label-filename').attr("for", 'upload'+id+'-filename');
      entry.find('.input-description').attr("id", 'upload'+id+'-description');
      entry.find('.label-description').attr("for", 'upload'+id+'-description');
      entry.find('.btn-up').on('click', function(){
        entry.prev().before(entry);
        renumberUploads();
      });
      entry.find('.btn-down').on('click', function(){
        entry.next().after(entry);
        renumberUploads();
      });
      entry.find('.btn-remove').on('click', function(){
        entry.remove();
        renumberUploads();
      });
      renumberUploads();
    },
    success: function(id, name, json){
      --uploading;
      var entry = $('#upload'+id).removeClass('uploading');
      name = name.replace(/\.\w+$/, "."+json.Extension);
      entry.find('.input-key').attr("value", json.Key);
      entry.find('.extension').text("."+json.Extension);
      $('<div class="alert alert-info"><a></a></div>')
        .find('a').attr({
          href: '/tmp/' + json.Key + '/' + name,
          target: "_blank"
        }).text(name + "(" + formatReadableFilesize(json.Size) + ") のプレビュー").end()
        .appendTo(entry.find('.upload-status').empty());
    },
    error: function(id, name, err){
      --uploading;
      var entry = $('#upload'+id).removeClass('uploading').addClass('disabled');
      entry.find('.btn-up, .btn-down').addClass('disabled');
      entry.find('.fileinfo').remove();
      $('<div class="alert alert-danger">').text(name + " のアップロードに失敗しました: " + err.Error).appendTo(entry.find('.upload-status').empty());
    }
  });

  sse();

/*
  $('audio').mediaelementplayer({
    audioWidth: 112,
    audioHeight: 30,
    enableAutosize: true,
    features: ['playpause','volume']
  });
  $('div.player').affix();
*/
});

function formLock(sw){
  var f = $('#message-form'), fe = f.get(0);
  sw = !!sw;
  fe.Name.disabled = sw;
  fe.Body.disabled = sw;
  f.find('button[type=submit]').get(0).disabled = sw;
  $('#upload-container').toggleClass('disabled', sw);
}

function renumberUploads(){
  var uploadList = $('#upload-list').children(':not(.disabled)').each(function(idx){
    var $this = $(this);
    $this.find('.input-key').attr("name", "Uploads."+idx+".Key");
    $this.find('.input-filename').attr("name", "Uploads."+idx+".Name");
    $this.find('.input-description').attr("name", "Uploads."+idx+".Description");
  });
}

function sse(is3G){
  var url = document.body.getAttribute('data-sse');
  if (!url) return;

  if (is3G){
    url += "3g";
  }

  var es = new EventSource(url);
  if (is3G === undefined){
    //3秒以内に test イベントが返ってこない場合は
    //バッファリングされてしまっているので3Gモードに切り替える
    var timer;
    es.addEventListener('open', function(e) {
      timer = setTimeout(function(){
        timer = null;
        es.close();
        sse(true);
      }, 3000);
    });
    es.addEventListener('test', function(e) {
      if (timer){
        clearTimeout(timer);
        timer = null;
      }
    }, false);
  }
  es.addEventListener('message', function(e) {
    $.each($($.parseJSON(e.data)), function(){
      addLog(this);
    });
  }, false);
  es.addEventListener('update', function(e) {
    $.each($($.parseJSON(e.data)), function(){
      addLog(this, false, true);
    });
  }, false);
}

function updateMore(){
  var more = $('#more');
  var l = $('#messages').children('div:last-child');
  if (l.siblings().length == 0 || l.data('index') == 1){
    more.empty();
  } else {
    var e = $('<button type="button" class="btn btn-default btn-lg btn-block">続きを読む</button>');
    e.on('click', function(){
      $(this).attr('disabled', 'disabled').text('Loading...');
      var mount = document.body.getAttribute('data-mount');
      $.get('/'+mount+'/'+"-"+($('#messages > div:last-child').data('index')-1), undefined, undefined, "text").done(function(r){
        $.each($($.trim(r)), function(){
          addLog(this, true);
        });
        updateMore();
      });
      return false;
    });
    more.empty().append(e);
  }
}

function addLog(msg, append, updateOnly){
  var elem = $(msg);
  var idx = elem.data('index')
  var old = $('#msg'+idx);
  if (old.length){
    old.replaceWith(elem);
  } else {
    if (updateOnly){
      return;
    }

    var messages = $("#messages");
    //挿入の場合は連番が維持できる場合のみ実行する
    if (append){
      if ($('#msg'+(idx+1)).length || !messages.children().length){
        messages.append(elem);
      }
    } else {
      if ($('#msg'+(idx-1)).length || !messages.children().length){
        messages.prepend(elem);
      }
    }
  }
  processLogPost(elem);
}

function processLogPost(elem){
  if (elem.data('processed')){
    return;
  }
  elem.data('processed', 1);

  var audioListen = elem.find('.btn-listen');
  if (audioListen.length){
    audioListen.on('click', function(){
      var $this = $(this), parent = $this.parent();
      var type = $this.data('type'), src = $this.attr('href');
      var audio = $('<audio preload="none" src="'+src+'"></audio>').appendTo(parent.empty());
      audio.mediaelementplayer({
        audioWidth: 200,
        audioHeight: 30,
        enableAutosize: false,
        features: ['playpause','progress','volume'],
        pauseOtherPlayers: true,
        success: function (mediaElement, domObject) { 
          setTimeout(function(){
            mediaElement.play();
          }, 0);
        }
      });
      return false;
    });
  }
  /*
  var popovers = elem.find('a[data-res]');
  if (popovers.length){
    popovers.each(function(){
      var $this = $(this);
      var content = 'Loading...';
      $this.popover({
        html: true,
        content: function(){
          return content;
        }
      }).on('show.bs.popover', function(){
        if ($this.data('loaded')) return;
        $this.data('loaded', true);
        $.get($this.data('res')).done(function(json){
          var e = $('<div>');
          $.each(json, function(){
            addLog(this, true, e);
          });
          content = e;
          $this.popover('show');
        });
      });
    });
  }
  */
}
