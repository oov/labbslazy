window.onunload = function(){}; //戻るのキャッシュを無効化

var uploading = 0, posting = 0;
$(function(){
  var uploadTemplate = $('#upload-template').html();
  var uploadList = $('#upload-list');
  $('#legacy-upload').remove();
  $('#upload-container').show().upload({
    uploading: function(id, name){
      ++uploading;
      var entry = $(uploadTemplate).addClass('uploading').attr('id', 'upload'+id).appendTo(uploadList);
      entry.find('.input-filename').attr({
        "id": 'upload'+id+'-filename',
        "value": name.replace(/\.\w+$/, '')
      });
      entry.find('.label-filename').attr("for", 'upload'+id+'-filename');
      entry.find('.input-description').attr("id", 'upload'+id+'-description');
      entry.find('.label-description').attr("for", 'upload'+id+'-description');
      entry.find('.btn-up').on('click', function(){
        entry.prev().before(entry);
        renumberUploads();
      }).text('↑');
      entry.find('.btn-down').on('click', function(){
        entry.next().after(entry);
        renumberUploads();
      }).text('↓');
      entry.find('.btn-remove').on('click', function(){
        entry.remove();
        renumberUploads();
      }).text('☓');
      renumberUploads();
    },
    success: function(id, name, json){
      --uploading;
      var entry = $('#upload'+id).removeClass('uploading');
      name = name.replace(/\.\w+$/, "."+json.Extension);
      entry.find('.input-key').attr("value", json.Key);
      entry.find('.extension').text("."+json.Extension);
      $('<div class="alert alert-info"><a></a></div>')
        .find('a').attr({
          href: '/tmp/' + json.Key + '/' + name,
          target: "_blank"
        }).text(name + "(" + formatReadableFilesize(json.Size) + ") のプレビュー").end()
        .appendTo(entry.find('.upload-status').empty());
    },
    error: function(id, name, err){
      --uploading;
      var entry = $('#upload'+id).removeClass('uploading').addClass('disabled');
      entry.find('.btn-up, .btn-down').addClass('disabled');
      entry.find('.fileinfo').remove();
      $('<div class="alert alert-danger">').text(name + " のアップロードに失敗しました: " + err.Error).appendTo(entry.find('.upload-status').empty());
    },
    inputStyle: null,
    labelStyle: null
  });

});

function renumberUploads(){
  var uploadList = $('#upload-list').children(':not(.disabled)').each(function(idx){
    var $this = $(this);
    $this.find('.input-key').attr("name", "Uploads."+idx+".Key");
    $this.find('.input-filename').attr("name", "Uploads."+idx+".Name");
    $this.find('.input-description').attr("name", "Uploads."+idx+".Description");
  });
}

