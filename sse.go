package main

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"bitbucket.org/oov/labbslazy/log"
	"bitbucket.org/oov/labbslazy/sse"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	"github.com/gorilla/mux"
	"net/http"
	"strings"
	"time"
)

type broadcastVars struct {
	Mount     string
	MountInfo *log.MountInfo
	Messages  []*log.Message
	HasMore   int64
	IsBasic   bool
}

func broadcastMessage(eventType string, id int64, message *log.Message) error {
	return broadcastMessages(eventType, id, message.Parent(), []*log.Message{message})
}

func broadcastMessages(eventType string, id int64, l *log.Log, messages []*log.Message) error {
	mn, err := buildMessageNotify(eventType, id, l, messages)
	if err != nil {
		return err
	}
	return l.Broadcast(mn)
}

func buildMessageNotify(eventType string, id int64, l *log.Log, messages []*log.Message) (*log.MessageNotify, error) {
	vars := broadcastVars{
		Mount:     l.Mount(),
		MountInfo: l.MountInfo(),
		Messages:  messages,
	}

	buf := bytes.NewBufferString("")
	err := tmpl.ExecuteTemplate(buf, "message.html", vars)
	if err != nil {
		return nil, err
	}
	buf2 := bytes.NewBufferString("")
	json.NewEncoder(buf2).Encode(strings.TrimSpace(buf.String()))
	return &log.MessageNotify{
		EventType: eventType,
		Id:        id,
		JSON:      buf2.Bytes(),
	}, nil
}

func sseHandler(conn *sse.Conn, r *http.Request) {
	m := mux.Vars(r)
	mount := m["mount"]
	is3G := m["3g"] != ""

	if !validMountPoint(mount) {
		return
	}

	l, err := logPool.GetExist(mount)
	if err == kvs.ErrNotExist {
		return
	}
	if err != nil {
		glog.Errorln(err)
		return
	}

	ch := make(chan *log.MessageNotify)

	err = l.AddListener(ch)
	if err != nil {
		glog.Errorln(err)
		return
	}
	defer l.RemoveListener(ch)

	//データを補完する処理
	var lastEventId int64
	_, err = fmt.Sscanf(r.Header.Get("Last-Event-Id"), "%d", &lastEventId)
	if err == nil && lastEventId != 0 {
		go func() {
			//自分自身も含まれるので+1した上で最初以外を使う
			messages, err := l.GetMulti(fmt.Sprintf("%d-", lastEventId), LOG_PAGE_SIZE+1)
			if err != nil {
				return
			}

			if len(messages) > 1 {
				mn, err := buildMessageNotify("", messages[len(messages)-1].Index, l, messages[1:])
				if err != nil {
					return
				}
				ch <- mn
			}
		}()
	}

	//クライアント側でバッファリングされずに受信できるか
	//テストデータを送信してチェックする
	conn.Write("event", "test")
	conn.WriteFlush("data", "1")

	for {
		select {
		case <-time.After(60 * time.Second):
			conn.WriteFlush("", "")
		case mn := <-ch:
			if mn.Id != 0 {
				conn.Write("id", fmt.Sprintf("%d", mn.Id))
			}
			if mn.EventType != "" {
				conn.Write("event", mn.EventType)
			}
			conn.WriteFlush("data", string(mn.JSON))
		case <-conn.CloseNotify():
			return
		}
		if r.Header.Get("X-Requested-With") == "XMLHttpRequest" || is3G {
			conn.WriteFlush("retry", "5000")
			return
		}
	}
}
