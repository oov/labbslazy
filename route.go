package main

import (
	"bitbucket.org/oov/labbslazy/logger"
	"bitbucket.org/oov/labbslazy/sse"
	"github.com/gorilla/mux"
	"net/http"
)

func route() {
	r := mux.NewRouter()
	r.StrictSlash(true)

	r.HandleFunc("/tmp", logger.Handler(uploadPostHandler)).Methods("POST")
	r.HandleFunc("/tmp/{key:u[A-Za-z0-9]+\\.[A-Za-z0-9]+}/{name:[^/]+}", logger.Handler(uploadGetHandler)).Methods("GET")

	var b, s *mux.Router

	s = r.PathPrefix("/mount/").Subrouter()
	s.HandleFunc("/", mountIndexHandler)
	s.HandleFunc("/{mount:[A-Za-z][A-Za-z0-9._-]*}/", mountEditHandler)

	//トップ
	r.HandleFunc("/", logger.Handler(topHandler)).Methods("GET")
	r.HandleFunc("/{basic:b}/", logger.Handler(topHandler)).Methods("GET")

	//通常ページ
	r.HandleFunc("/{mount:[A-Za-z][A-Za-z0-9._-]*}/", logger.Handler(indexHandler)).Methods("GET")
	s = r.PathPrefix("/{mount:[A-Za-z][A-Za-z0-9._-]*}/").Subrouter()
	s.HandleFunc("/post", logger.Handler(postHandler)).Methods("POST")
	s.Handle("/sse{3g:(3g)?}", sse.Handler(sseHandler)).Methods("GET")
	s.HandleFunc("/{n:[0-9,-]+}", logger.Handler(partialHandler)).Methods("GET")
	s.Handle("/{n:[0-9]+}/{id:[A-Za-z0-9._-]+}/{mode:dl\\/|th\\/|}{name:[^/]+}", logger.Handler(fileHandler)).Methods("GET")
	s.HandleFunc("/{n:[0-9]+}/delete", logger.Handler(deleteHandler))

	//ベーシックモード
	b = r.PathPrefix("/{basic:b}").Subrouter()
	b.StrictSlash(true)
	b.HandleFunc("/{mount:[A-Za-z][A-Za-z0-9._-]*}/", logger.Handler(indexHandler)).Methods("GET")
	s = b.PathPrefix("/{mount:[A-Za-z][A-Za-z0-9._-]*}/").Subrouter()
	s.HandleFunc("/post", logger.Handler(postHandler)).Methods("POST")
	s.HandleFunc("/{n:[0-9,-]+}", logger.Handler(partialHandler)).Methods("GET")
	s.HandleFunc("/{n:[0-9]+}/delete", logger.Handler(deleteHandler))

	r.Handle("/r/{file:.+}", http.FileServer(http.Dir("pub/")))
	http.Handle("/", r)
}
