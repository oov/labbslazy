package logger

import (
	"encoding/json"
	"flag"
	"github.com/golang/glog"
	"github.com/najeira/ltsv"
	"net/http"
	"os"
	"time"
)

var (
	logname = flag.String("access", "access.log", "access log filename")
	log     = make(chan map[string]string, 100)
	notify  = make(chan bool, 0)
)

func Close() {
	notify <- true
}

func Rotate() {
	notify <- false
}

type Handler func(w http.ResponseWriter, r *http.Request)

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h(w, r)
	if *logname == "" {
		return
	}

	host := r.Header.Get("X-Real-IP")
	if host == "" {
		host = r.RemoteAddr
		if host == "" {
			host = "-"
		}
	}
	referer := r.Referer()
	if referer == "" {
		referer = "-"
	}
	ua := r.UserAgent()
	if ua == "" {
		ua = "-"
	}
	log <- map[string]string{
		"host":    host,
		"time":    time.Now().Format("[02/Jan/2006:15:04:05 -0700]"),
		"req":     r.Method + " " + r.RequestURI + " " + r.Proto,
		"referer": referer,
		"ua":      ua,
	}
}

func logging() {
	var f *os.File
	var err error
	var w *ltsv.Writer
	for {
		select {
		case n := <-notify:
			if w != nil {
				w.Flush()
				/*
					if err != nil {
						glog.Errorln("cannot flush access log:", err)
					}
				*/
				w = nil
			}
			if f != nil {
				err = f.Close()
				if err != nil {
					glog.Errorln("cannot close access log:", err)
				}
				f = nil
			}
			if n {
				return
			}
		case d := <-log:
			if f == nil {
				f, err = os.OpenFile(*logname, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
				if err != nil {
					glog.Fatalln("cannot create access log:", err)
				}
				_, err = f.Seek(0, os.SEEK_END)
				if err != nil {
					glog.Fatalln("cannot seek access log:", err)
				}
				w = ltsv.NewWriter(f)
			}

			err = w.Write(d)
			if err != nil {
				//可能ならエラーを起こしたデータ自体も見れるようにログを残す
				b, err2 := json.Marshal(d)
				if err2 != nil {
					glog.Fatalln("cannot write access log:", err)
				} else {
					glog.Fatalln("cannot write access log:", err, string(b))
				}
			}
		}
	}
}

func rot() {
	ch := make(chan struct{})
	go rotate(ch)
	for _ = range ch {
		Rotate()
	}
}

func init() {
	go logging()
	go rot()
}
