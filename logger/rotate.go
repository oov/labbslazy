// +build !windows

package logger

import (
	"os"
	"os/signal"
	"syscall"
)

func rotate(rotateCh chan struct{}) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGUSR1)
	for _ = range c {
		rotateCh <- struct{}{}
	}
}
