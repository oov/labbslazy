package main

import (
	"bitbucket.org/oov/labbslazy/log"
	"bitbucket.org/oov/labbslazy/pool"
	"errors"
	"github.com/golang/glog"
	"regexp"
)

var (
	validMountPointRE = regexp.MustCompile(`^[a-zA-Z0-9\-._]+$`)
)

func validMountPoint(s string) bool {
	return validMountPointRE.MatchString(s) && s != "tmp" && s != "mount" && s != "r" && s != "b"
}

type MessageParam struct {
	Mount string
	No    int64
}

type Api struct {
	pool *pool.LogPool
}

func (api *Api) mount(mount string) (*log.Log, error) {
	if !validMountPoint(mount) {
		return nil, errors.New("invalid mount point")
	}

	l, err := api.pool.GetExist(mount)
	if err != nil {
		return nil, err
	}

	return l, nil
}

func (api *Api) get(mount string, n int64) (*log.Message, error) {
	l, err := api.mount(mount)
	if err != nil {
		return nil, err
	}

	return l.Get(n)
}

func (api *Api) Get(m *MessageParam, msg *log.Message) error {
	ret, err := api.get(m.Mount, m.No)
	if err != nil {
		return err
	}

	*msg = *ret
	return nil
}

func (api *Api) delete(mount string, n int64) error {
	msg, err := api.get(mount, n)
	if err != nil {
		return err
	}

	err = msg.Parent().Delete(msg)
	if err != nil {
		return err
	}

	glog.Infof("Mount:%s No.%d deleted.", mount, n)

	//削除した時はイベントをブロードキャストする
	return broadcastMessage("update", 0, msg)
}

func (api *Api) Delete(m *MessageParam, deleted *bool) error {
	err := api.delete(m.Mount, m.No)
	if err != nil {
		return err
	}

	*deleted = true
	return nil
}

func (api *Api) latest(mount string) (*log.Log, []*log.Message, error) {
	l, err := api.mount(mount)
	if err != nil {
		return nil, nil, err
	}

	return l, l.GetLatest(), nil
}

func (api *Api) Latest(mount string, msgs *([]*log.Message)) error {
	_, ret, err := api.latest(mount)
	if err != nil {
		return err
	}

	*msgs = ret
	return nil
}

func (api *Api) generateDetail(mount string, n int64) error {
	msg, err := api.get(mount, n)
	if err != nil {
		return err
	}

	if msg.Files != nil && len(msg.Files) > 0 {
		//添付ファイルがある場合

		//サムネイルの生成などを行う
		err = msg.GenerateDetail(awsBucket)
		if err != nil {
			return err
		}
	}

	//更新内容を書き込み
	err = msg.Parent().Update(msg)
	if err != nil {
		return err
	}

	glog.Infof("Mount:%s No.%d detail generated.", mount, n)

	//更新したことを配信
	return broadcastMessage("update", 0, msg)
}

func (api *Api) GenerateDetail(m *MessageParam, success *bool) error {
	err := api.generateDetail(m.Mount, m.No)
	if err != nil {
		return err
	}

	*success = true
	return nil
}
