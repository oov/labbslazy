package thumb

import (
	//"github.com/nfnt/resize"
	"bitbucket.org/oov/labbslazy/resize"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"io"
)

func Thumbnail(r io.Reader, w io.Writer, size int) error {
	img, _, err := image.Decode(r)
	if err != nil {
		return err
	}

	var nw, nh int
	bounds := img.Bounds()
	sz := bounds.Size()
	if sz.X > sz.Y {
		nw = size
		nh = sz.Y * size / sz.X
	} else {
		nh = size
		nw = sz.X * size / sz.Y
	}

	//th := resize.Resize(nw, nh, img, resize.Bilinear)
	th := resize.Resize(img, bounds, nw, nh)
	return jpeg.Encode(w, th, &jpeg.Options{Quality: 90})
}
