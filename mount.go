package main

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"bitbucket.org/oov/labbslazy/log"
	"errors"
	"flag"
	"github.com/golang/glog"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"net/http"
)

var (
	masterPassword = flag.String("password", "hackme", "master password")
)

type mountIndexPost struct {
	Mount    string //対象マウントポイント
	Password string //マスターパスワード
	Edit     bool   //編集モードか
	Remove   bool   //削除モードか
}

type mountIndexVars struct {
	Deleted bool   //削除済みか
	Error   error  //エラー発生時の内容
	Mount   string //処理対象のマウントポイント
}

func mountIndexHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	pd := new(mountIndexPost)
	err = schema.NewDecoder().Decode(pd, r.PostForm)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	vars := mountIndexVars{
		Mount: pd.Mount,
	}

	switch {
	case pd.Edit:
		if !validMountPoint(pd.Mount) {
			vars.Error = errors.New("マウントポイント名が不正です")
			break
		}

		http.Redirect(w, r, "/mount/"+pd.Mount+"/", http.StatusFound)
		return
	case pd.Remove:
		if !validMountPoint(pd.Mount) {
			vars.Error = errors.New("マウントポイント名が不正です")
			break
		}

		if len(pd.Password) < 8 {
			vars.Error = errors.New("パスワードが短すぎます")
			break
		}

		if *masterPassword != pd.Password {
			vars.Error = errors.New("パスワードが不正です")
			break
		}

		//TODO: 削除処理の実行

		vars.Deleted = true
	}

	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	err = tmpl.ExecuteTemplate(w, "mount.html", vars)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

type mountEditPost struct {
	Mount       string //対象マウントポイント
	Password    string //マスターパスワード
	Title       string
	Description string
	TripSalt    string
}

type mountEditVars struct {
	Mount     string
	MountInfo log.MountInfo
	Error     error
}

func mountEditHandler(w http.ResponseWriter, r *http.Request) {
	m := mux.Vars(r)

	mount := m["mount"]
	if !validMountPoint(mount) {
		http.Error(w, "invalid mount point", http.StatusForbidden)
		return
	}

	l, err := logPool.GetExist(mount)
	if err != nil && err != kvs.ErrNotExist {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	err = r.ParseForm()
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	pd := new(mountEditPost)
	err = schema.NewDecoder().Decode(pd, r.PostForm)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	vars := mountEditVars{
		Mount: mount,
	}

	if l != nil {
		vars.MountInfo = *l.MountInfo()
	}

	switch {
	case pd.Mount == mount:
		vars.MountInfo.Title = pd.Title
		vars.MountInfo.Description = pd.Description
		if pd.TripSalt != "" {
			vars.MountInfo.TripSalt = pd.TripSalt
		}
		if len(pd.Password) < 8 {
			vars.Error = errors.New("パスワードが短すぎます")
			break
		}

		if *masterPassword != pd.Password {
			vars.Error = errors.New("パスワードが不正です")
			break
		}

		if len(pd.Title) > 64 {
			vars.Error = errors.New("タイトルが長すぎます")
			break
		}

		if len(pd.Description) > 1024 {
			vars.Error = errors.New("説明が長すぎます")
			break
		}

		if l == nil {
			l, err = logPool.Get(mount)
			if err != nil {
				glog.Errorln(err)
				http.Error(w, "internal server error", http.StatusInternalServerError)
				return
			}
			if l == nil {
				http.Error(w, "internal server error", http.StatusInternalServerError)
				return
			}
		}

		err = l.SetMountInfo(&vars.MountInfo)
		if err != nil {
			glog.Errorln(err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			break
		}

		http.Redirect(w, r, "/"+mount+"/", 302)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	err = tmpl.ExecuteTemplate(w, "mount-edit.html", vars)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}
