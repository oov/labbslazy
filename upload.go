package main

import (
	"encoding/json"
	"github.com/golang/glog"
	"github.com/gorilla/mux"
	"html/template"
	"net/http"
	"os"
	"strings"
)

type UploadError struct {
	Error string
}

func uploadGetHandler(w http.ResponseWriter, r *http.Request) {
	m := mux.Vars(r)
	reader, err := uploader.Reader(m["key"])
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	h := w.Header()
	h.Set("Content-Type", reader.Mime())
	h.Set("X-Content-Type-Options", "nosniff")
	h.Set("X-Frame-Options", "SAMEORIGIN")
	h.Set("X-XSS-Protection", "1; mode=block")
	http.ServeContent(w, r, "", reader.ModDate(), reader)
}

func uploadPostHandler(w http.ResponseWriter, r *http.Request) {
	const POST_MAX_SIZE = 20 * 1024 * 1024
	defer r.Body.Close()

	if r.ContentLength > POST_MAX_SIZE {
		putUploadResponse(UploadError{"送信データサイズが大きすぎます"}, w, r)
		return
	}

	err := r.ParseMultipartForm(POST_MAX_SIZE)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	f, fh, err := r.FormFile("file")
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	defer f.Close()

	var buf [16]byte
	n, err := f.Read(buf[:])
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	mime := detectMimeType(buf[:n], fh.Filename)
	if mime == "application/octet-stream" {
		putUploadResponse(UploadError{"未対応のファイル形式です"}, w, r)
		return
	}

	extension := mimeToExtension(mime)

	_, err = f.Seek(0, os.SEEK_SET)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	key, size, err := uploader.Upload(f, mime)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	putUploadResponse(map[string]interface{}{
		"Key":       key,
		"Mime":      mime,
		"Size":      size,
		"Extension": extension,
	}, w, r)
}

func putUploadResponse(data interface{}, w http.ResponseWriter, r *http.Request) {
	h := w.Header()
	h.Set("X-Content-Type-Options", "nosniff")
	h.Set("X-Frame-Options", "SAMEORIGIN")
	h.Set("X-XSS-Protection", "1; mode=block")
	if r.Header.Get("X-Requested-With") == "XMLHttpRequest" {
		w.Header().Set("Content-Type", "application/json")
		err := json.NewEncoder(w).Encode(data)
		if err != nil {
			glog.Errorln(err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
	} else {
		buf, err := json.Marshal(data)
		if err != nil {
			glog.Errorln(err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "text/html; charset=UTF-8")
		w.Write([]byte("<!DOCTYPE html><html><body><pre>"))
		template.HTMLEscape(w, buf)
		w.Write([]byte("</pre></body></html>"))
	}
}

func mimeToExtension(mime string) string {
	switch mime {
	case "image/jpeg":
		return "jpg"
	case "image/gif":
		return "gif"
	case "image/png":
		return "png"
	case "audio/mp3":
		return "mp3"
	case "application/ogg":
		return "ogg"
	case "audio/mpeg":
		return "m4a"
	case "audio/midi":
		return "mid"
	case "application/x-vsq":
		return "vsq"
	case "application/x-vsqx":
		return "vsqx"
	case "application/zip":
		return "zip"
	case "application/x-7z-compressed":
		return "7z"
	case "text/plain":
		return "txt"
	}
	return ""
}

func detectMimeType(head []byte, filename string) string {
	n := len(head)
	ext3 := strings.ToLower(filename[len(filename)-4:])
	switch {
	case n >= 2 && head[0] == 0xff && head[1] == 0xd8:
		return "image/jpeg"
	case n >= 4 && head[0] == 0x47 && head[1] == 0x49 && head[2] == 0x46 && head[3] == 0x38:
		return "image/gif"
	case n >= 4 && head[0] == 0x89 && head[1] == 0x50 && head[2] == 0x4E && head[3] == 0x47:
		return "image/png"

	case n >= 2 && head[0] == 0xff && head[1] == 0xfb:
		return "audio/mp3"
	case n >= 3 && head[0] == 0x49 && head[1] == 0x44 && head[2] == 0x33 && ext3 == ".mp3":
		return "audio/mp3"
	case n >= 4 && head[0] == 0x4f && head[1] == 0x67 && head[2] == 0x67 && head[3] == 0x53 && (ext3 == ".ogg" || ext3 == ".oga"):
		return "application/ogg"
	case n >= 12 && head[4] == 0x66 && head[5] == 0x74 && head[6] == 0x79 && head[7] == 0x70 && head[8] == 0x4d && head[9] == 0x34 && head[10] == 0x41 && head[11] == 0x20 && (ext3 == ".mp4" || ext3 == ".m4a"):
		return "audio/mpeg"

	case n >= 4 && head[0] == 0x4D && head[1] == 0x54 && head[2] == 0x68 && head[3] == 0x64 && (ext3 == ".mid"):
		return "audio/midi"
	case n >= 4 && head[0] == 0x4D && head[1] == 0x54 && head[2] == 0x68 && head[3] == 0x64 && (ext3 == ".vsq"):
		return "application/x-vsq"
	case n >= 5 && head[0] == 0x3C && head[1] == 0x3F && head[2] == 0x78 && head[3] == 0x6D && head[4] == 0x6C && (strings.ToLower(filename[len(filename)-5:]) == ".vsqx"):
		return "application/x-vsqx"

	case n >= 4 && head[0] == 0x50 && head[1] == 0x4b && head[2] == 0x03 && head[3] == 0x04:
		return "application/zip"
	case n >= 7 && head[0] == 0x37 && head[1] == 0x7a && head[2] == 0xbc && head[3] == 0xaf && head[5] == 0x27 && head[6] == 0x1c:
		return "application/x-7z-compressed"

	case ext3 == ".txt":
		return "text/plain"
	}

	return "application/octet-stream"
}
