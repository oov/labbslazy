package sse

import (
	"github.com/golang/glog"
	"net/http"
)

type Conn struct {
	w http.ResponseWriter
	http.Flusher
	http.CloseNotifier
}

func (c Conn) WriteFlush(key, message string) {
	c.w.Write([]byte(key + ": " + message + "\n\n"))
	c.Flush()
}

func (c Conn) Write(key, message string) {
	c.w.Write([]byte(key + ": " + message + "\n"))
}

type Handler func(*Conn, *http.Request)

func (h Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	f, f_ok := w.(http.Flusher)

	if !f_ok {
		glog.Errorln("ResponseWriter is not a Flusher")
		http.Error(w, "internal server error", http.StatusInternalServerError)
	}

	cn, cn_ok := w.(http.CloseNotifier)

	if !cn_ok {
		glog.Errorln("ResponseWriter is not a CloseNotifier")
		http.Error(w, "internal server error", http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Transfer-Encoding", "chunked")
	w.Header().Set("X-Accel-Buffering", "no")

	f.Flush()

	h(&Conn{
		w:             w,
		Flusher:       f,
		CloseNotifier: cn,
	}, req)
}
