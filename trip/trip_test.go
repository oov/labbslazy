package trip

import (
	"testing"
)

func TestTrip(t *testing.T) {
	if EncodedName("hello#world", "salt") != "hello◆SEbfGb0bdY" {
		t.Fail()
	}
}
