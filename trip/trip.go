package trip

import (
	"crypto/md5"
	"encoding/base64"
	"strings"
)

var (
	signatureEncoding = base64.NewEncoding("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./")
)

func EncodedName(source, salt string) string {
	pos := strings.Index(source, "#")
	if pos == -1 {
		return strings.Replace(source, "◆", "◇", -1)
	}

	m := md5.New()
	m.Write([]byte(source[pos+1:] + salt))

	var b64 [24]byte
	signatureEncoding.Encode(b64[:], m.Sum(nil))

	return strings.Replace(source[:pos], "◆", "◇", -1) + "◆" + string(b64[:10])
}
