package main

import (
	"bitbucket.org/oov/labbslazy/chunk"
	"bitbucket.org/oov/labbslazy/cli"
	"bitbucket.org/oov/labbslazy/kvs"
	"bitbucket.org/oov/labbslazy/log"
	"bitbucket.org/oov/labbslazy/logger"
	"bitbucket.org/oov/labbslazy/pool"
	"bitbucket.org/oov/labbslazy/upload"
	"errors"
	"flag"
	"fmt"
	"github.com/crowdmob/goamz/aws"
	"github.com/crowdmob/goamz/s3"
	"github.com/golang/glog"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"html/template"
	"io"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"
)

const (
	LOG_PAGE_SIZE = 50
	CHUNK_SIZE    = 256 * 1024 //same as GridFS
)

var (
	logPool  = pool.NewLogPool("db/", LOG_PAGE_SIZE, CHUNK_SIZE)
	api      = &Api{pool: logPool}
	uploader = upload.Must(upload.New(logPool.Pool(), "tmp", CHUNK_SIZE))

	awsRegion     = flag.String("region", "", "AWS Region")
	awsBucketName = flag.String("bucket", "", "Amazon S3 BucketName")
	awsBucket     *s3.Bucket
)

func cc(s string) template.HTML {
	return template.HTML(s)
}

func urlbase(isBasic bool) string {
	if isBasic {
		return "/b/"
	}
	return "/"
}

var tmpl = template.Must(template.New("").Funcs(template.FuncMap{
	"cc":      cc,
	"urlbase": urlbase,
}).ParseFiles(
	"tmpl/base-css.html",
	"tmpl/base-js.html",
	"tmpl/footer.html",
	"tmpl/index.html",
	"tmpl/partial.html",
	"tmpl/mount.html",
	"tmpl/mount-edit.html",
	"tmpl/delete.html",
	"tmpl/top.html",
	"tmpl/upload-template.html",
	"tmpl/message.html",
	"tmpl/navbar.html",
))

type topVars struct {
	IsBasic bool
}

func topHandler(w http.ResponseWriter, r *http.Request) {
	m := mux.Vars(r)

	vars := indexVars{
		IsBasic: m["basic"] != "",
	}

	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	err := tmpl.ExecuteTemplate(w, "top.html", vars)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

type indexVars struct {
	Mount     string
	MountInfo *log.MountInfo
	Messages  []*log.Message
	HasMore   int64
	IsBasic   bool
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	m := mux.Vars(r)
	mount := m["mount"]
	l, messages, err := api.latest(mount)
	if err == kvs.ErrNotExist {
		http.NotFound(w, r)
		return
	}
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	for i, ln, lnhalf := 0, len(messages), len(messages)/2; i < lnhalf; i++ {
		messages[i], messages[ln-1-i] = messages[ln-1-i], messages[i]
	}

	vars := indexVars{
		Mount:     mount,
		MountInfo: l.MountInfo(),
		Messages:  messages,
		IsBasic:   m["basic"] != "",
	}
	if messages != nil && len(messages) > 1 && messages[len(messages)-1].Index > 1 {
		vars.HasMore = messages[len(messages)-1].Index - 1
	}

	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	err = tmpl.ExecuteTemplate(w, "index.html", vars)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

type partialVars struct {
	Mount     string
	Current   string
	MountInfo *log.MountInfo
	Messages  []*log.Message
	HasMore   int64
	IsBasic   bool
}

func partialHandler(w http.ResponseWriter, r *http.Request) {
	m := mux.Vars(r)

	mount := m["mount"]
	l, err := api.mount(mount)
	if err == kvs.ErrNotExist {
		http.NotFound(w, r)
		return
	}
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	messages, err := l.GetMulti(m["n"], LOG_PAGE_SIZE)
	if err != nil {
		if _, ok := err.(log.ErrorInvalidRange); ok {
			http.Error(w, "invalid request range", http.StatusForbidden)
			return
		}
		glog.Errorln(err)
		http.Error(w, "invalid request range", http.StatusForbidden)
		return
	}

	for i, ln, lnhalf := 0, len(messages), len(messages)/2; i < lnhalf; i++ {
		messages[i], messages[ln-1-i] = messages[ln-1-i], messages[i]
	}

	vars := partialVars{
		Mount:     mount,
		Current:   m["n"],
		MountInfo: l.MountInfo(),
		Messages:  messages,
		IsBasic:   m["basic"] != "",
	}
	if messages != nil && len(messages) > 1 && messages[len(messages)-1].Index > 1 {
		vars.HasMore = messages[len(messages)-1].Index - 1
	}

	h := w.Header()
	h.Set("X-Content-Type-Options", "nosniff")
	h.Set("X-Frame-Options", "SAMEORIGIN")
	h.Set("X-XSS-Protection", "1; mode=block")
	h.Set("Content-Type", "text/html; charset=UTF-8")

	var templateName string
	if r.Header.Get("X-Requested-With") == "XMLHttpRequest" {
		templateName = "message.html"
	} else {
		templateName = "partial.html"
	}
	err = tmpl.ExecuteTemplate(w, templateName, vars)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

type deletePost struct {
	Delete   bool
	Password string
}

type deleteVars struct {
	Mount     string
	MountInfo *log.MountInfo
	Messages  []*log.Message
	Message   *log.Message
	Error     error
	IsBasic   bool
}

func deleteHandler(w http.ResponseWriter, r *http.Request) {
	m := mux.Vars(r)

	mount := m["mount"]

	var n int64
	_, err := fmt.Sscanf(m["n"], "%d", &n)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	msg, err := api.get(mount, n)
	if err == kvs.ErrNotExist || err == kvs.ErrKVSKeyNotFound {
		http.NotFound(w, r)
		return
	} else if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	vars := deleteVars{
		Mount:     mount,
		MountInfo: msg.Parent().MountInfo(),
		Message:   msg,
		Messages:  []*log.Message{msg},
		IsBasic:   m["basic"] != "",
	}

	err = r.ParseForm()
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	pd := new(deletePost)
	err = schema.NewDecoder().Decode(pd, r.PostForm)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	switch {
	case pd.Delete:
		if pd.Password != *masterPassword {
			vars.Error = errors.New("パスワードが違います")
			break
		}

		err = api.delete(mount, n)
		if err != nil {
			glog.Errorln(err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
	}

	h := w.Header()
	h.Set("X-Content-Type-Options", "nosniff")
	h.Set("X-Frame-Options", "SAMEORIGIN")
	h.Set("X-XSS-Protection", "1; mode=block")
	h.Set("Content-Type", "text/html; charset=UTF-8")

	err = tmpl.ExecuteTemplate(w, "delete.html", vars)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

type PostData struct {
	Name         string
	Body         string
	LegacyUpload bool
	Uploads      []PostDataUpload
}

type PostDataUpload struct {
	Key         string
	Name        string
	Description string
	File        string
}

func postHandler(w http.ResponseWriter, r *http.Request) {
	const POST_MAX_SIZE = 20 * 1024 * 1024
	const NAME_MAX_SIZE = 128
	const BODY_MAX_SIZE = 12800

	m := mux.Vars(r)

	if r.Method != "POST" {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	if r.ContentLength > POST_MAX_SIZE {
		defer r.Body.Close()
		http.Error(w, "request too large", http.StatusForbidden)
		return
	}

	err := r.ParseMultipartForm(POST_MAX_SIZE)
	if err != nil && err != http.ErrNotMultipart {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	pd := new(PostData)
	if r.MultipartForm != nil {
		err = schema.NewDecoder().Decode(pd, r.MultipartForm.Value)
	} else {
		err = schema.NewDecoder().Decode(pd, r.PostForm)
	}
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		glog.Errorln(err)
		return
	}

	if strings.TrimSpace(pd.Body) == "" {
		http.Error(w, "message body must not be empty", http.StatusForbidden)
		return
	}

	if len(pd.Name) > NAME_MAX_SIZE {
		http.Error(w, "name too long", http.StatusForbidden)
		return
	}

	if len(pd.Body) > BODY_MAX_SIZE {
		http.Error(w, "message body too long", http.StatusForbidden)
		return
	}

	mount := m["mount"]
	l, err := api.mount(mount)
	if err == kvs.ErrNotExist {
		http.NotFound(w, r)
		return
	}
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		glog.Errorln(err)
		return
	}

	ip := r.Header.Get("X-Real-IP")
	if ip == "" {
		ip = r.RemoteAddr
	}
	msg := &log.Message{
		Body:  pd.Body,
		Name:  pd.Name,
		IP:    ip,
		Files: make([]*log.AttachFile, 0, len(pd.Uploads)),
	}

	//アップロードされているファイルの情報を追加する
	if len(pd.Uploads) > 0 {
		if pd.LegacyUpload {
			for idx, v := range pd.Uploads {
				f, fh, err := r.FormFile(fmt.Sprintf("Uploads.%d.File", idx))
				if err == http.ErrMissingFile {
					continue
				}
				if err != nil {
					glog.Errorln(err)
					http.Error(w, "internal server error", http.StatusInternalServerError)
					return
				}
				defer f.Close()

				var buf [16]byte
				n, err := f.Read(buf[:])
				if err != nil {
					glog.Errorln(err)
					http.Error(w, "internal server error", http.StatusInternalServerError)
					return
				}

				mime := detectMimeType(buf[:n], fh.Filename)
				if mime == "application/octet-stream" {
					putUploadResponse(UploadError{"未対応のファイル形式です"}, w, r)
					return
				}

				extension := mimeToExtension(mime)

				_, err = f.Seek(0, os.SEEK_SET)
				if err != nil {
					glog.Errorln(err)
					http.Error(w, "internal server error", http.StatusInternalServerError)
					return
				}

				filename := fh.Filename
				pos := strings.LastIndex(fh.Filename, ".")
				if pos != -1 {
					filename = filename[:pos]
				}
				af := &log.AttachFile{
					Description: v.Description,
					Name:        filename + "." + extension,
					Mime:        mime,
				}
				af.SetReader(f)
				msg.Files = append(msg.Files, af)
			}
		} else {
			var reader *chunk.ChunkReader
			for _, v := range pd.Uploads {
				reader, err = uploader.Reader(v.Key)
				if err != nil {
					glog.Errorln(err)
					http.Error(w, "internal server error", http.StatusInternalServerError)
					return
				}
				af := &log.AttachFile{
					Description: v.Description,
					Name:        v.Name + "." + mimeToExtension(reader.Mime()),
					Mime:        reader.Mime(),
				}
				af.SetReader(reader)
				msg.Files = append(msg.Files, af)
			}
		}
	}

	err = l.Put(msg)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	//掃除する
	go func() {
		if pd.LegacyUpload {
			return
		}
		for _, v := range pd.Uploads {
			err = uploader.Sweep(v.Key)
			if err != nil {
				glog.Errorln(err)
			}
		}
	}()

	err = broadcastMessage("", msg.Index, msg)
	if err != nil {
		glog.Errorln(err)
	}

	bgMessagePostProcess <- l

	h := w.Header()
	h.Set("X-Content-Type-Options", "nosniff")
	h.Set("X-Frame-Options", "SAMEORIGIN")
	h.Set("X-XSS-Protection", "1; mode=block")
	if r.Header.Get("X-Requested-With") == "XMLHttpRequest" {
		h.Set("Content-Type", "application/json; charset=UTF-8")
		_, err = w.Write([]byte("1"))
		if err != nil {
			glog.Errorln(err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
	} else {
		if m["basic"] == "" {
			http.Redirect(w, r, "/"+mount+"/", http.StatusFound)
		} else {
			http.Redirect(w, r, "/b/"+mount+"/", http.StatusFound)
		}
	}
}

func fileHandler(w http.ResponseWriter, r *http.Request) {
	m := mux.Vars(r)
	mount := m["mount"]

	var n int64
	_, err := fmt.Sscanf(m["n"], "%d", &n)
	if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	msg, err := api.get(mount, n)
	if err == kvs.ErrNotExist || err == kvs.ErrKVSKeyNotFound {
		http.NotFound(w, r)
		return
	} else if err != nil {
		glog.Errorln(err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	if msg.State == log.MESSAGE_STATE_DELETED {
		http.NotFound(w, r)
		return
	}

	id := m["id"]
	var af *log.AttachFile
	for _, v := range msg.Files {
		if v.Id == id {
			af = v
			break
		}
	}
	if af == nil || af.State == log.ATTACHFILE_STATE_DELETED {
		http.NotFound(w, r)
		return
	}

	mode := m["mode"]
	var reader io.ReadSeeker
	var date time.Time
	var contentType string
	switch mode {
	case "th/":
		if af.Mime != "image/jpeg" && af.Mime != "image/png" && af.Mime != "image/gif" {
			http.NotFound(w, r)
			return
		}

		reader, err = af.ThumbnailReader()
		if err == kvs.ErrKVSKeyNotFound {
			http.NotFound(w, r)
			return
		}
		if err != nil {
			glog.Errorln(err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		date = time.Unix(0, msg.AddDate)
		contentType = "image/jpeg"
	case "dl/", "":
		cr, err := af.Reader()
		if err != nil {
			glog.Errorln(err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
		reader = cr
		contentType = af.Mime
		date = cr.ModDate()

		if mode == "dl/" {
			w.Header().Set("Content-Disposition", "attachment")
		}
	}

	h := w.Header()
	h.Set("Content-Type", contentType)
	h.Set("X-Content-Type-Options", "nosniff")
	h.Set("X-Frame-Options", "SAMEORIGIN")
	h.Set("X-XSS-Protection", "1; mode=block")
	http.ServeContent(w, r, "", date, reader)
}

func readyBucket() error {
	if *awsBucketName == "" {
		return errors.New("BucketName is empty")
	}
	if *awsRegion == "" {
		return errors.New("AWS Region is empty")
	}

	auth, err := aws.EnvAuth()
	if err != nil {
		return err
	}

	rg, ok := aws.Regions[*awsRegion]
	if !ok {
		return errors.New("invalid AWS Region: " + *awsRegion)
	}

	awsBucket = s3.New(auth, rg).Bucket(*awsBucketName)
	return nil
}

func main() {
	flag.Parse()

	if flag.NArg() != 1 || flag.Arg(0) != "server" {
		cli.CliMain()
		return
	}

	glog.Infoln("labbslazy running in server mode")

	if err := readyBucket(); err != nil {
		glog.Warningln("cannot backup to Amazon S3:", err)
	}

	defer logPool.Close()
	defer logger.Close()

	go bg()
	go cleanUpUploadFiles()

	route()
	listen(api)

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)
	for _ = range sig {
		return
	}
}
