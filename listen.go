package main

import (
	"flag"
	"fmt"
	"github.com/golang/glog"
	"net"
	"net/http"
	"net/http/fcgi"
	"net/rpc"
	"os"
	"strings"
)

var (
	rpcserv  = flag.String("rpc", "tcp://127.0.0.1:22233", "RPC Server Address")
	fcgiserv = flag.String("fcgi", "tcp://127.0.0.1:10081", "FastCGI Server Address")
	httpserv = flag.String("http", ":10080", "HTTP Server Address")
)

func listen(api *Api) {
	if *rpcserv != "" {
		go func() {
			err := listenAndServeRpc(api, *rpcserv)
			if err != nil {
				glog.Fatalln("Cannot start rpc server:", err)
			}
		}()
	}
	if *fcgiserv != "" {
		go func() {
			err := listenAndServeFcgi(*fcgiserv)
			if err != nil {
				glog.Fatalln("Cannot start fcgi server:", err)
			}
		}()
	}
	if *httpserv != "" {
		go func() {
			err := http.ListenAndServe(*httpserv, nil)
			if err != nil {
				glog.Fatalln("Cannot start http server:", err)
			}
		}()
	}
}

func listenAndServeRpc(api *Api, server string) (err error) {
	var s string
	var l net.Listener

	if strings.HasPrefix(server, "unix://") {
		s = (server)[7:]
		os.Remove(s)
		l, err = net.Listen("unix", s)
		os.Chmod(s, 0666)
	} else if strings.HasPrefix(server, "tcp://") {
		s = (server)[6:]
		l, err = net.Listen("tcp", s)
	} else {
		err = fmt.Errorf("unknown server bind address")
	}

	if err != nil {
		return
	}

	err = rpc.Register(api)
	if err != nil {
		return
	}
	rpc.HandleHTTP()
	return http.Serve(l, nil)
}

func listenAndServeFcgi(server string) (err error) {
	var s string
	var l net.Listener

	if strings.HasPrefix(server, "unix://") {
		s = (server)[7:]
		os.Remove(s)
		l, err = net.Listen("unix", s)
		os.Chmod(s, 0666)
	} else if strings.HasPrefix(server, "tcp://") {
		s = (server)[6:]
		l, err = net.Listen("tcp", s)
	} else {
		err = fmt.Errorf("unknown server bind address")
	}

	if err != nil {
		return
	}

	return fcgi.Serve(l, nil)
}
