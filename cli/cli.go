package cli

import (
	"bitbucket.org/oov/labbslazy/log"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/golang/glog"
	"net/rpc"
	"os"
)

type MessageParam struct {
	Mount string
	No    int64
}

func CliMain() {
	flag.Parse()

	c, err := rpc.DialHTTP("tcp", "127.0.0.1:22233")
	if err != nil {
		glog.Errorln("could not initialize rpc:", err.Error())
		return
	}

	args := flag.Args()
	var cmd string
	if len(args) > 0 {
		cmd = args[0]
	}

	switch cmd {
	case "msg":
		msg(args[1:], c)
	default:
		fmt.Println(`"` + os.Args[0] + `" is labbslazy remote controller.

Usage:

        ` + os.Args[0] + ` command [options]

The commands are:

        msg    manipulate messages
        server start in server mode
`)
	}
}

func msg(args []string, c *rpc.Client) {
	var cmd string
	if len(args) > 0 {
		cmd = args[0]
	}

	switch cmd {
	case "get":
		msgGet(args[1:], c)
	case "del":
		msgDelete(args[1:], c)
	case "gen":
		msgGenerateDetail(args[1:], c)
	default:
		fmt.Println(`"` + os.Args[0] + ` msg" is a command which manipulate message.

Usage:

        ` + os.Args[0] + ` msg sub-command [options]

The sub-commands are:

        get     get message
        del     delete message
        gen     generate detail
`)
	}
}

func msgGet(args []string, c *rpc.Client) {
	if len(args) < 2 {
		fmt.Println(`"` + os.Args[0] + ` msg get" is a command which read message data.

Usage:

        ` + os.Args[0] + ` msg get mountpoint number

Example:

        ` + os.Args[0] + ` msg get foobar 2
`)
		return
	}
	var msg *log.Message
	mount := args[0]

	var n int64
	_, err := fmt.Sscanf(args[1], "%d", &n)
	if err != nil {
		fmt.Println("Could not convert response number:", err)
		return
	}

	err = c.Call("Api.Get", &MessageParam{
		Mount: mount,
		No:    n,
	}, &msg)
	if err != nil {
		glog.Errorln("Api.Get Failed:", err)
		return
	}

	buf, err := json.MarshalIndent(msg, "", "  ")
	if err != nil {
		glog.Errorln("could not marshal rpc response:", err)
		return
	}

	fmt.Println(string(buf))
}

func msgDelete(args []string, c *rpc.Client) {
	if len(args) < 2 {
		fmt.Println(`"` + os.Args[0] + ` msg del" is a command which delete message.

Usage:

        ` + os.Args[0] + ` msg del mountpoint number

Example:

        ` + os.Args[0] + ` msg del foobar 2
`)
		return
	}
	var deleted bool
	mount := args[0]

	var n int64
	_, err := fmt.Sscanf(args[1], "%d", &n)
	if err != nil {
		fmt.Println("Could not convert response number:", err)
		return
	}

	err = c.Call("Api.Delete", &MessageParam{
		Mount: mount,
		No:    n,
	}, &deleted)
	if err != nil {
		glog.Errorln("Api.Delete Failed:", err)
		return
	}

	buf, err := json.MarshalIndent(deleted, "", "  ")
	if err != nil {
		glog.Errorln("could not marshal rpc response:", err)
		return
	}

	fmt.Println(string(buf))
}

func msgGenerateDetail(args []string, c *rpc.Client) {
	if len(args) < 2 {
		fmt.Println(`"` + os.Args[0] + ` msg gen" is a command which generate message detail.

Usage:

        ` + os.Args[0] + ` msg gen mountpoint number

Example:

        ` + os.Args[0] + ` msg gen foobar 2
`)
		return
	}
	var success bool
	mount := args[0]

	var n int64
	_, err := fmt.Sscanf(args[1], "%d", &n)
	if err != nil {
		fmt.Println("Could not convert response number:", err)
		return
	}

	err = c.Call("Api.GenerateDetail", &MessageParam{
		Mount: mount,
		No:    n,
	}, &success)
	if err != nil {
		glog.Errorln("Api.GenerateDetail Failed:", err)
		return
	}

	buf, err := json.MarshalIndent(success, "", "  ")
	if err != nil {
		glog.Errorln("could not marshal rpc response:", err)
		return
	}

	fmt.Println(string(buf))
}
