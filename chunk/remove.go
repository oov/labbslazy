package chunk

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"strconv"
)

func Remove(kvs kvs.KVS, id string) error {
	var m meta
	err := kvs.GetMsgpack([]byte(id), &m)
	if err != nil {
		return err
	}

	chunk := int64(m.Chunk)
	batch := kvs.NewBatch()

	for i := int64(0); i*chunk < m.Size; i++ {
		err = batch.Delete([]byte(id + "." + strconv.FormatInt(i, 10)))
		if err != nil {
			return err
		}
	}

	batch.Delete([]byte(id))
	if err != nil {
		return err
	}

	return batch.Write()
}
