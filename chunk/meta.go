package chunk

type meta struct {
	Size    int64
	ModDate int64
	Chunk   int
	Mime    string
	Name    string `codec:",omitempty"`
}
