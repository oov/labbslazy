package chunk

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"strconv"
	"time"
)

//io.WriteCloser を実装した kvs.KVS 用の GridFS みたいな奴
type ChunkWriter struct {
	id    string
	meta  meta
	batch kvs.Batch
	kvs   kvs.KVS
	buf   []byte
	cur   []byte
}

func NewChunkWriter(kvs kvs.KVS, id string, chunk int, mime string) *ChunkWriter {
	buf := make([]byte, chunk)
	return &ChunkWriter{
		id: id,
		meta: meta{
			Size:  0,
			Chunk: chunk,
			Mime:  mime,
		},
		batch: kvs.NewBatch(),
		kvs:   kvs,
		buf:   buf,
		cur:   buf,
	}
}

func (c *ChunkWriter) Write(p []byte) (n int, err error) {
	var clen, plen, i, l int
	for clen, plen, n = len(c.cur), len(p), 0; plen > 0; clen, plen = len(c.cur), len(p) {
		if clen == 0 {
			err = c.batch.Set([]byte(c.id+"."+strconv.FormatInt(c.meta.Size/int64(c.meta.Chunk)-1, 10)), c.buf)
			if err != nil {
				return
			}
			c.cur = c.buf
			clen = len(c.cur)
		}

		if clen <= plen {
			l = clen
		} else {
			l = plen
		}
		for i = 0; i < l; i++ {
			c.cur[i] = p[i]
		}
		c.cur = c.cur[l:]
		p = p[l:]
		n += l
		c.meta.Size += int64(l)
	}
	return
}

func (c *ChunkWriter) Close() error {
	var err error
	if last := c.meta.Chunk - len(c.cur); last > 0 {
		err = c.batch.Set([]byte(c.id+"."+strconv.FormatInt(c.meta.Size/int64(c.meta.Chunk), 10)), c.buf[:last])
		if err != nil {
			return err
		}
	}

	c.meta.ModDate = time.Now().UnixNano()
	err = c.batch.SetMsgpack([]byte(c.id), c.meta)
	if err != nil {
		return err
	}

	return c.batch.Write()
}

func (c *ChunkWriter) Batch() kvs.Batch {
	return c.batch
}
