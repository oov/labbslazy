package chunk

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"errors"
	"io"
	"os"
	"strconv"
	"time"
)

//io.ReadSeeker を実装した kvs.KVS 用の GridFS みたいな奴
type ChunkReader struct {
	id      string
	meta    meta
	pos     int64
	current []byte
	kvs     kvs.KVS
}

func NewChunkReader(kvs kvs.KVS, id string) (c *ChunkReader, err error) {
	var m meta
	err = kvs.GetMsgpack([]byte(id), &m)
	if err != nil {
		return
	}

	c = &ChunkReader{
		id:      id,
		meta:    m,
		pos:     0,
		current: []byte{},
		kvs:     kvs,
	}
	return
}

func (c *ChunkReader) Read(p []byte) (n int, err error) {
	var clen, plen, i, l int
	for n, plen, clen = 0, len(p), len(c.current); plen > 0; plen, clen = len(p), len(c.current) {
		//現在のチャンクからはもう読めない
		if clen == 0 {
			//読み取り位置が終点に到達
			//もう読めないので終了
			if c.pos == c.meta.Size {
				if n == 0 {
					//1バイトも読めてない場合は io.EOF
					err = io.EOF
				}
				return
			}

			//新しいチャンクを準備
			idx := c.pos / int64(c.meta.Chunk)
			c.current, err = c.kvs.Get([]byte(c.id + "." + strconv.FormatInt(idx, 10)))
			if err != nil {
				return
			}

			c.current = c.current[c.pos-idx*int64(c.meta.Chunk):]
			clen = len(c.current)
		}

		if plen <= clen {
			//現在のチャンクから必要なデータを全部読める
			l = plen
		} else {
			//今回のチャンクでは必要なデータを全部は読めない
			l = clen
		}
		for i = 0; i < l; i++ {
			p[i] = c.current[i]
		}
		p = p[l:]
		c.current = c.current[l:]
		n += l
		c.pos += int64(l)
	}
	return
}

func (c *ChunkReader) Seek(offset int64, whence int) (ret int64, err error) {
	switch whence {
	case os.SEEK_SET:
		offset = offset
	case os.SEEK_CUR:
		offset = c.pos + offset
	case os.SEEK_END:
		offset = c.meta.Size + offset
	}
	if offset < 0 || offset > c.meta.Size {
		return 0, errors.New("chunk: out of range")
	}
	c.pos = offset
	c.current = []byte{}
	return c.pos, nil
}

func (c *ChunkReader) Size() int64 {
	return c.meta.Size
}

func (c *ChunkReader) Mime() string {
	return c.meta.Mime
}

func (c *ChunkReader) ModDate() time.Time {
	return time.Unix(0, c.meta.ModDate)
}
