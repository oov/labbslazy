package log

import (
	"github.com/crowdmob/goamz/s3"
	"html/template"
	"time"
)

const (
	MESSAGE_STATE_UNDEFINED = iota
	MESSAGE_STATE_COMPLETE
	MESSAGE_STATE_DELETED
	MESSAGE_STATE_GENERATING_ATTACHFILE_INFORMATION
)

type Message struct {
	parent     *Log          `codec:"-"`
	Index      int64         `codec:"-"`          //書き込み番号
	Id         string        `codec:",omitempty"` //内部的な固有識別子
	Name       string        `codec:",omitempty"` //表示用の名前
	Body       string        ``                   //メッセージ本文
	State      int           `codec:",omitempty"` //削除フラグなど
	AddDate    int64         `codec:",omitempty"` //投稿日時
	IP         string        ``                   //投稿者IP
	DeleteDate int64         `codec:",omitempty"` //削除日時
	Files      []*AttachFile `codec:",omitempty"` //添付ファイル
}

func (m *Message) IsDeleted() bool {
	return m.State == MESSAGE_STATE_DELETED
}

func (m *Message) AddDateReadable() string {
	if m.AddDate != 0 {
		return time.Unix(0, m.AddDate).Format("2006-01-02 15:04:05.000")
	} else {
		return "0000-00-00 00:00:00.000"
	}
}

func (m *Message) BodyHTML(resBaseUrl string) template.HTML {
	return template.HTML(autolink(m.Body, resBaseUrl))
}

func (m *Message) Parent() *Log {
	return m.parent
}

//画像のサムネイルなどを生成する
//bucket が渡された場合はS3へのバックアップも行う
func (m *Message) GenerateDetail(bucket *s3.Bucket) error {
	var err error
	ch := make(chan error, 2)
	for _, file := range m.Files {
		//S3へのバックアップとサムネイルなどの作成は同時に行う
		go func() {
			if bucket != nil {
				ch <- file.BackupToS3(bucket)
			} else {
				ch <- nil
			}
		}()
		go func() {
			ch <- file.GenerateDetail()
		}()
		if err = <-ch; err != nil {
			return err
		}
		if err = <-ch; err != nil {
			return err
		}
	}
	if m.State == MESSAGE_STATE_GENERATING_ATTACHFILE_INFORMATION {
		m.State = MESSAGE_STATE_COMPLETE
	}
	return nil
}
