package log

import (
	"bitbucket.org/oov/labbslazy/chunk"
	"bitbucket.org/oov/labbslazy/kvs"
	"bitbucket.org/oov/labbslazy/trip"
	"container/ring"
	"fmt"
	"io"
	"strings"
	"sync"
	"time"
)

const (
	THUMBNAIL_SIZE = 128
)

type ErrorInvalidRange error

type MessageNotify struct {
	data      interface{}
	EventType string
	Id        int64
	JSON      []byte
}

type Log struct {
	mount string
	pool  kvs.Pool
	chunk int

	mountInfo MountInfo

	hot    *ring.Ring
	ringrw sync.RWMutex

	listener   map[chan<- *MessageNotify]struct{}
	listenerrw sync.RWMutex
}

func (log *Log) Mount() string {
	return log.mount
}

func (log *Log) setWithEncode(name string, key []byte, data interface{}, updateOnly bool) error {
	var db kvs.KVS
	var err error
	if updateOnly {
		db, err = log.pool.GetExist(name)
	} else {
		db, err = log.pool.Get(name)
	}
	if err != nil {
		return err
	}

	return db.SetMsgpack(key, data)
}

func (log *Log) getWithDecode(name string, key []byte, data interface{}) error {
	db, err := log.pool.GetExist(name)
	if err != nil {
		return err
	}

	return db.GetMsgpack(key, data)
}

func (log *Log) Count() (int64, error) {
	var n int64

	err := log.getWithDecode(generalDBName(log.mount), countKey(), &n)
	if err != nil {
		return 0, err
	}

	return n, nil
}

func (log *Log) setCount(count int64) error {
	err := log.setWithEncode(generalDBName(log.mount), countKey(), count, false)
	if err != nil {
		return err
	}

	return nil
}

func (log *Log) CompleteCount() (int64, error) {
	var n int64

	err := log.getWithDecode(generalDBName(log.mount), completeCountKey(), &n)
	if err != nil {
		return 0, err
	}

	return n, nil
}

func (log *Log) SetCompleteCount(count int64) error {
	err := log.setWithEncode(generalDBName(log.mount), completeCountKey(), count, false)
	if err != nil {
		return err
	}

	return nil
}

func (log *Log) getMountInfo() (*MountInfo, error) {
	var mi MountInfo
	err := log.getWithDecode(generalDBName(log.mount), mountInfoKey(), &mi)
	if err != nil {
		return nil, err
	}

	return &mi, nil
}

func (log *Log) setMountInfo(mi *MountInfo) error {
	err := log.setWithEncode(generalDBName(log.mount), mountInfoKey(), mi, false)
	if err != nil {
		return err
	}

	return nil
}

func (log *Log) SetMountInfo(mi *MountInfo) error {
	err := log.setMountInfo(mi)
	if err != nil {
		return err
	}
	log.mountInfo = *mi
	return nil
}

func (log *Log) MountInfo() *MountInfo {
	return &log.mountInfo
}

func (log *Log) Title() string {
	return log.mountInfo.Title
}

func (log *Log) Description() string {
	return log.mountInfo.Description
}

func (log *Log) get(n int64) (*Message, error) {
	var msg Message

	err := log.getWithDecode(messageDBName(log.mount, n), messageKey(n), &msg)
	if err != nil {
		return nil, err
	}

	msg.Index = n
	msg.parent = log
	for i := range msg.Files {
		msg.Files[i].parent = &msg
	}

	return &msg, nil
}

func (log *Log) put(n int64, msg *Message, updateOnly bool) error {
	var db kvs.KVS
	var err error
	if updateOnly {
		db, err = log.pool.GetExist(messageDBName(log.mount, n))
	} else {
		db, err = log.pool.Get(messageDBName(log.mount, n))
	}
	if err != nil {
		return err
	}

	var size int64
	for _, v := range msg.Files {
		if v.Id == "" {
			v.Id = uploadKey(n)
			writer := chunk.NewChunkWriter(db, v.Id, log.chunk, v.Mime)
			size, err = io.Copy(writer, v.reader)
			if err != nil {
				return err
			}

			err = writer.Close()
			if err != nil {
				return err
			}
			v.Size = size
		}
	}

	err = log.setWithEncode(messageDBName(log.mount, n), messageKey(n), msg, updateOnly)
	if err != nil {
		return err
	}

	msg.parent = log
	return nil
}

//最近の投稿に関するデータをDBから読み取ってリングバッファを形成する
func (log *Log) getFilledHotLog(ringLen int, count int64) (*ring.Ring, error) {
	ringBuf := ring.New(ringLen)
	var msg *Message
	var err error

	i := count - int64(ringLen)
	if i < 1 {
		i = 1
	}
	for ; i <= count; i++ {
		msg, err = log.get(i)
		if err == kvs.ErrNotExist || err == kvs.ErrKVSKeyNotFound {
			break
		}
		if err != nil {
			return nil, err
		}

		ringBuf.Value = msg
		ringBuf = ringBuf.Next()
	}
	return ringBuf, nil
}

func New(pool kvs.Pool, mount string, hotLogSize int, chunk int, createIfMissing bool) (*Log, error) {
	log := &Log{
		pool:     pool,
		mount:    mount,
		chunk:    chunk,
		listener: make(map[chan<- *MessageNotify]struct{}),
	}

	if !createIfMissing {
		if _, err := log.pool.GetExist(generalDBName(mount)); err != nil {
			return nil, err
		}
	}

	mi, err := log.getMountInfo()
	if err != nil {
		log.setMountInfo(&log.mountInfo)
	} else {
		log.mountInfo = *mi
	}

	count, err := log.Count()
	if err != nil {
		log.setCount(0)
	}

	_, err = log.CompleteCount()
	if err != nil {
		log.SetCompleteCount(0)
	}

	log.hot, err = log.getFilledHotLog(hotLogSize, count)
	if err != nil {
		return nil, err
	}

	return log, nil
}

func (log *Log) Listeners() int {
	log.listenerrw.RLock()
	defer log.listenerrw.RUnlock()
	return len(log.listener)
}

func (log *Log) AddListener(ch chan<- *MessageNotify) error {
	log.listenerrw.Lock()
	defer log.listenerrw.Unlock()
	log.listener[ch] = struct{}{}
	return nil
}

func (log *Log) RemoveListener(ch chan<- *MessageNotify) error {
	log.listenerrw.Lock()
	defer log.listenerrw.Unlock()
	delete(log.listener, ch)
	return nil
}

func (log *Log) Broadcast(mn *MessageNotify) error {
	log.listenerrw.RLock()
	defer log.listenerrw.RUnlock()
	for k, _ := range log.listener {
		k <- mn
	}
	return nil
}

func (log *Log) Update(msg *Message) error {
	err := log.put(msg.Index, msg, true)
	if err != nil {
		return err
	}

	return nil
}

func (log *Log) Delete(msg *Message) error {
	if msg.State == MESSAGE_STATE_DELETED {
		return nil
	}

	msg.State = MESSAGE_STATE_DELETED
	msg.DeleteDate = time.Now().UnixNano()
	return log.Update(msg)
}

func (log *Log) Put(msg *Message) error {
	count, err := log.Count()
	if err != nil {
		return err
	}

	count++

	err = log.setCount(count)
	if err != nil {
		return err
	}

	msg.State = MESSAGE_STATE_GENERATING_ATTACHFILE_INFORMATION
	msg.Index = count
	msg.AddDate = time.Now().UnixNano()
	msg.parent = log
	msg.Name = trip.EncodedName(msg.Name, log.mountInfo.TripSalt)
	for _, v := range msg.Files {
		v.State = ATTACHFILE_STATE_GENERATING_ATTACHFILE_INFORMATION
		v.parent = msg
	}

	log.ringrw.Lock()
	cur := log.hot
	log.hot = cur.Next()
	cur.Value = msg
	log.ringrw.Unlock()

	err = log.put(count, msg, false)
	if err != nil {
		return err
	}

	return nil
}

func (log *Log) Get(n int64) (r *Message, err error) {
	//先にリングバッファの中を探す
	log.ringrw.RLock()
	log.hot.Do(func(p interface{}) {
		if r != nil {
			return
		}
		if msg, ok := p.(*Message); ok && msg.Index == n {
			r = msg
		}
	})
	log.ringrw.RUnlock()
	if r != nil {
		return r, nil
	}

	//見つからない時はDBを見に行く
	return log.get(n)
}

//2 や 2- や -3 や 2-3 のような表記からレス番号を抽出する
//この解析では , を含む形式には対応しない
func (log *Log) parseResNumber(r []int64, s string, max int, count int64) ([]int64, error) {
	strs := strings.SplitN(s, "-", 2)
	switch len(strs) {
	//値がひとつの場合はそのデータのみを返す
	case 1:
		var n int64
		_, err := fmt.Sscanf(strs[0], "%d", &n)
		if err != nil {
			return nil, ErrorInvalidRange(err)
		}
		if count < n || n <= 0 {
			return nil, ErrorInvalidRange(fmt.Errorf("%d is invalid range", n))
		}
		return append(r, n), nil

	//値がふたつの場合はその範囲のデータを返す
	case 2:
		var from, to int64
		if strs[0] != "" {
			_, err := fmt.Sscanf(strs[0], "%d", &from)
			if err != nil {
				return nil, ErrorInvalidRange(err)
			}
			if count < from || from <= 0 {
				return nil, ErrorInvalidRange(fmt.Errorf("%d is invalid range", from))
			}
		}
		if strs[1] != "" {
			_, err := fmt.Sscanf(strs[1], "%d", &to)
			if err != nil {
				return nil, ErrorInvalidRange(err)
			}
			if count < to || to <= 0 {
				return nil, ErrorInvalidRange(fmt.Errorf("%d is invalid range", to))
			}
		}

		//返すべきデータの範囲を求める
		if from != 0 {
			if to != 0 {
				if from > to {
					from, to = to, from
				}
			} else {
				to = from + int64(max)
				if to > count {
					to = count
				}
			}
		} else if to != 0 {
			from = to - int64(max) + 1
			if from < 1 {
				from = 1
			}
		}

		for ; len(r) < max && from <= to; from++ {
			r = append(r, from)
		}
		return r, nil
	default:
		return nil, ErrorInvalidRange(fmt.Errorf("invalid parameter(%s)", s))
	}
}

//2ch 風のレス表記の数字部分を表したテキストを元に該当投稿を推測して返す
//  >>2
//  >>2-3
//  >>2,4
//上記形式に対応
func (log *Log) GetMulti(s string, max int) (r []*Message, err error) {
	count, err := log.Count()
	if err != nil {
		return nil, err
	}

	nums := make([]int64, 0, max)
	strs := strings.SplitN(s, ",", max)
	for i, ln := 0, len(strs); i < ln && len(nums) < max; i++ {
		nums, err = log.parseResNumber(nums, strs[i], max, count)
		if err != nil {
			return nil, err
		}
	}

	var msg *Message
	r = make([]*Message, len(nums))
	for i, ln := 0, len(nums); i < ln; i++ {
		msg, err = log.Get(nums[i])
		if err != nil {
			return nil, err
		}
		r[i] = msg
	}
	return r, nil
}

func (log *Log) GetLatest() []*Message {
	r := make([]*Message, 0)
	log.ringrw.RLock()
	log.hot.Do(func(p interface{}) {
		if msg, ok := p.(*Message); ok && msg.Index > 0 {
			r = append(r, msg)
		}
	})
	log.ringrw.RUnlock()
	return r
}
