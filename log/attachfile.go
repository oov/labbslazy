package log

import (
	"bitbucket.org/oov/labbslazy/chunk"
	"bitbucket.org/oov/labbslazy/thumb"
	"bytes"
	"errors"
	"github.com/crowdmob/goamz/aws"
	"github.com/crowdmob/goamz/s3"
	"io"
	"strconv"
	"strings"
)

const (
	ATTACHFILE_STATE_UNDEFINED = iota
	ATTACHFILE_STATE_COMPLETE
	ATTACHFILE_STATE_DELETED
	ATTACHFILE_STATE_GENERATING_ATTACHFILE_INFORMATION
)

var ()

type AttachFile struct {
	parent      *Message  `codec:"-"`
	Id          string    ``          //データ本体が格納されているキー
	Name        string    ``          //ファイル名
	Description string    ``          //ファイルの説明
	Mime        string    ``          //Mime タイプ
	Size        int64     ``          //ファイルサイズ
	reader      io.Reader `codec:"-"` //保存時にのみ使用
	State       int       ``          //削除フラグなど
	S3Path      string
}

func (af *AttachFile) Parent() *Message {
	return af.parent
}

func (af *AttachFile) IsDeleted() bool {
	return af.State == ATTACHFILE_STATE_DELETED
}

func (af *AttachFile) IsImage() bool {
	return af.Mime[0:6] == "image/"
}

func (af *AttachFile) IsMP3() bool {
	return af.Mime == "audio/mp3"
}

func (af *AttachFile) HasThumbnail() bool {
	return af.IsImage() && af.State == ATTACHFILE_STATE_COMPLETE
}

func (af *AttachFile) SizeReadable() string {
	suffix := []string{
		"Bytes",
		"KB",
		"MB",
		"GB",
		"TB",
	}
	sz, i, n := float64(af.Size), 0, 0
	for sz > 1000 {
		i++
		sz /= 1024.0
	}
	if i != 0 && sz < 100 {
		n++
	}
	if i != 0 && sz < 10 {
		n++
	}

	return strconv.FormatFloat(sz, 'f', n, 64) + " " + suffix[i]
}

func (af *AttachFile) GenerateDetail() error {
	switch af.Mime {
	case "image/gif", "image/png", "image/jpeg":
		r, err := af.Reader()
		if err != nil {
			return err
		}

		buf := bytes.NewBufferString("")
		err = thumb.Thumbnail(r, buf, THUMBNAIL_SIZE)
		if err != nil {
			return err
		}

		msg := af.parent
		log := msg.parent
		kvs, err := log.pool.GetExist(messageDBName(log.mount, msg.Index))
		if err != nil {
			return err
		}

		err = kvs.Set([]byte(thumbnailKey(af.Id)), buf.Bytes())
		if err != nil {
			return err
		}

		if af.State == ATTACHFILE_STATE_GENERATING_ATTACHFILE_INFORMATION {
			af.State = ATTACHFILE_STATE_COMPLETE
		}
	}
	return nil
}

func (af *AttachFile) buildS3BackupPath() string {
	//"mountname/34/1234/greiugrhkarjfuiuh"
	path := af.Parent().Parent().Mount()
	path += "/"
	path += strconv.FormatInt(af.Parent().Index%100, 10)
	path += "/"
	path += strconv.FormatInt(af.Parent().Index, 10)
	path += "/"
	path += af.Id
	return path
}

func (af *AttachFile) BackupToS3(bucket *s3.Bucket) error {
	r, err := af.Reader()
	if err != nil {
		return err
	}

	//"mountname/34/1234/greiugrhkarjfuiuh"
	path := af.buildS3BackupPath()
	err = bucket.PutReader(path, r, af.Size, af.Mime, s3.Private, s3.Options{
		Meta: map[string][]string{
			"mount": []string{af.Parent().Parent().Mount()},
			"no":    []string{strconv.FormatInt(af.Parent().Index, 10)},
		},
	})
	if err != nil {
		return err
	}

	af.S3Path = bucket.Region.Name + ":" + bucket.Name + ":" + path
	return nil
}

//保存時に使用する Reader を割り当てるためのメソッド
func (af *AttachFile) SetReader(r io.Reader) {
	af.reader = r
}

//S3に保存されているデータを Reader で読み取る
func (af *AttachFile) S3Reader(auth aws.Auth) (io.ReadCloser, error) {
	ss := strings.SplitN(af.S3Path, ":", 3)
	if len(ss) != 3 {
		return nil, errors.New("S3Path is empty")
	}
	bucket := s3.New(auth, aws.Regions[ss[0]]).Bucket(ss[1])
	return bucket.GetReader(ss[2])
}

func (af *AttachFile) Reader() (*chunk.ChunkReader, error) {
	msg := af.parent
	log := msg.parent

	kvs, err := log.pool.GetExist(messageDBName(log.mount, msg.Index))
	if err != nil {
		return nil, err
	}

	return chunk.NewChunkReader(kvs, af.Id)
}

func (af *AttachFile) ThumbnailReader() (*bytes.Reader, error) {
	msg := af.parent
	log := msg.parent

	kvs, err := log.pool.GetExist(messageDBName(log.mount, msg.Index))
	if err != nil {
		return nil, err
	}

	buf, err := kvs.Get([]byte(thumbnailKey(af.Id)))
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(buf), nil
}
