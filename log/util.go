package log

import (
	"math/rand"
	"strconv"
)

//投稿を保存するためのDB名を組み立てて返す
func messageDBName(mount string, n int64) string {
	num := strconv.FormatInt(n/10000, 36)
	ln := len(num)
	for i := 4 - ln; i > 0; i-- {
		num = "0" + num
	}
	return mount + "/l" + num
}

//投稿を保存する際のDB内のキー名を組み立てて返す
func messageKey(n int64) []byte {
	return []byte("m" + strconv.FormatInt(n, 36))
}

//このマウントポイントにおける全般的な情報を格納するためのDB名を組み立てて返す
func generalDBName(mount string) string {
	return mount + "/stat"
}

//総投稿数を格納するためのキー名を返す
func countKey() []byte {
	return []byte("count")
}

//マウントポイントに関する情報を格納するためのキー名を返す
func mountInfoKey() []byte {
	return []byte("mount-info")
}

//ポストプロセスが完了した投稿数を格納するためのキー名を返す
func completeCountKey() []byte {
	return []byte("complete-count")
}

//アップロードされたファイルを格納するためのキー名をランダムに組み立てて返す
func uploadKey(n int64) string {
	const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	const LENGTH = 24
	s := "m" + strconv.FormatInt(n, 36) + "."
	for i, cl := 0, len(chars); i < LENGTH; i++ {
		s += string(chars[rand.Intn(cl)])
	}
	return s
}

//アップロードキーに対応したサムネイル用のキー名を返す
func thumbnailKey(uploadKey string) string {
	return "thumb " + uploadKey
}
