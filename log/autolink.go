package log

import (
	"html/template"
	"regexp"
)

var (
	reAutolink = regexp.MustCompile(`(https?|ftp):\/\/[\x21\x23-\x26\x28-\x3b\x3d\x3f-\x7e]+`)
	reRes      = regexp.MustCompile(`>>((?:[0-9]{1,}\-(?:[0-9]{1,})?|[0-9]{1,})(?:,[0-9]{1,}\-(?:[0-9]{1,})?|,[0-9]{1,})*)`)
	reNl2br    = regexp.MustCompile(`\r\n|\r|\n`)
)

//テキストを解析して URL とレスにリンクを貼る
func autolink(s, resBaseUrl string) string {
	var ret, url string
	var pos int
	for _, v := range reAutolink.FindAllStringSubmatchIndex(s, -1) {
		url = template.HTMLEscapeString(s[v[0]:v[1]])
		ret += res(s[pos:v[0]], resBaseUrl)
		ret += `<a href="` + url + `" target="_blank">` + url + `</a>`
		pos = v[1]
	}
	ret += res(s[pos:], resBaseUrl)
	return ret
}

func res(s, resBaseUrl string) string {
	var ret string
	var pos int
	resBaseUrl = template.HTMLEscapeString(resBaseUrl)
	for _, v := range reRes.FindAllStringSubmatchIndex(s, -1) {
		ret += nl2br(s[pos:v[0]])
		ret += `<a href="` + resBaseUrl + template.HTMLEscapeString(s[v[2]:v[3]]) + `" target="_blank">` + template.HTMLEscapeString(s[v[0]:v[1]]) + `</a>`
		pos = v[1]
	}
	ret += nl2br(s[pos:])
	return ret
}

func nl2br(s string) string {
	return reNl2br.ReplaceAllString(template.HTMLEscapeString(s), "<br>")
}
