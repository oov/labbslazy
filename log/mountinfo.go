package log

import (
	"html/template"
)

type MountInfo struct {
	Title       string
	Description string
	TripSalt    string
}

func (m *MountInfo) DescriptionHTML(resBaseUrl string) template.HTML {
	return template.HTML(autolink(m.Description, resBaseUrl))
}
