package main

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"bitbucket.org/oov/labbslazy/log"
	"github.com/golang/glog"
	"time"
)

var (
	bgMessagePostProcess = make(chan *log.Log, 20)
)

func cleanUpUploadFiles() error {
	var err error
	for {
		err = uploader.SweepOldFiles(6 * time.Hour)
		if err != nil {
			glog.Errorln(err)
		}
		time.Sleep(5 * time.Minute)
	}
}

func bg() {
	var lg *log.Log
	for {
		select {
		case lg = <-bgMessagePostProcess:
			processMessage(lg)
		}
	}
}

func processMessage(l *log.Log) error {
	c, err := l.Count()
	if err != nil {
		glog.Errorln(err)
		return err
	}

	cc, err := l.CompleteCount()
	if err != nil {
		glog.Errorln(err)
		return err
	}

	if c == cc {
		return nil
	}

	defer func() {
		bgMessagePostProcess <- l
	}()

	cc++

	err = api.generateDetail(l.Mount(), cc)
	if err == kvs.ErrKVSKeyNotFound {
		//プログラムのバグなどで欠番が発生している
		//どうせデータ自体が存在していないので削除扱いにする
		err = l.Delete(&log.Message{
			Index: cc,
		})
		if err != nil {
			glog.Errorln(err)
			return err
		}
	} else if err != nil {
		glog.Errorln(err)
		return err
	}

	err = l.SetCompleteCount(cc)
	if err != nil {
		glog.Errorln(err)
		return err
	}
	return nil
}
