package kvs

import (
	"bytes"
	"errors"
	"github.com/ugorji/go/codec"
)

var (
	//KVS の Get でキーに対応したデータがない場合に返すべきエラー
	ErrKVSKeyNotFound = errors.New("key not found")
)

//データベースへの書き込みを行うインターフェイス
type Setter interface {
	Set(key, value []byte) error                    //データを保存する
	SetMsgpack(key []byte, value interface{}) error //データを MessagePack でエンコードして保存する
}

func SetMsgpack(s Setter, key []byte, value interface{}) error {
	buf := bytes.NewBufferString("")
	err := codec.NewEncoder(buf, &codec.MsgpackHandle{}).Encode(&value)
	if err != nil {
		return err
	}

	return s.Set(key, buf.Bytes())
}

type Getter interface {
	Get(key []byte) ([]byte, error)                 //保存したデータを読み込む
	GetMsgpack(key []byte, value interface{}) error //MessagePack でエンコードされたデータを読み込む
}

func GetMsgpack(g Getter, key []byte, value interface{}) error {
	buf, err := g.Get(key)
	if err != nil {
		return err
	}

	return codec.NewDecoderBytes(buf, &codec.MsgpackHandle{}).Decode(value)
}

type Deleter interface {
	Delete(key []byte) error //登録されているデータを削除する
}

//データベースへの書き込みをアトミックな単位で行うためのインターフェイス
type Batch interface {
	Setter
	Deleter
	Write() error //データベースに内容を反映する
}

//データベースへのアクセスに使用する key-value 型データストアのインターフェイス。
//
//Get(key []byte) でキーに登録されたデータがない場合、エラーは kvs.ErrKVSKeyNotFound を返すこと。
type KVS interface {
	Setter
	Getter
	Deleter
	Close() error //データベースを閉じる
	NewBatch() Batch
}
