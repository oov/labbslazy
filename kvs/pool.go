package kvs

import (
	"errors"
)

var (
	//KVS の GetExist でデータがない場合に返すべきエラー
	ErrNotExist = errors.New("kvs: not exist in pool")
)

type Pool interface {
	Get(name string) (KVS, error)
	GetExist(name string) (KVS, error)
}
