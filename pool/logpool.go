package pool

import (
	"bitbucket.org/oov/labbslazy/log"
	"sync"
	"time"
)

type LogPool struct {
	pool        *Pool
	logPool     map[string]*logPoolItem
	closeNotify chan<- struct{}
	numLog      int
	chunkSize   int
	m           sync.Mutex
}

func NewLogPool(baseDir string, numLog int, chunkSize int) *LogPool {
	ch := make(chan struct{}, 0)
	r := &LogPool{
		pool:        NewPool(baseDir),
		logPool:     make(map[string]*logPoolItem),
		numLog:      numLog,
		chunkSize:   chunkSize,
		closeNotify: ch,
	}
	go r.gc(ch)
	return r
}

func (lp *LogPool) Close() {
	lp.closeNotify <- struct{}{}
	lp.pool.Close()
}

func (lp *LogPool) Pool() *Pool {
	return lp.pool
}

type logPoolItem struct {
	l          *log.Log
	lastAccess time.Time
}

func (lp *LogPool) get(name string, createIsMissing bool) (*logPoolItem, error) {
	lpi, ok := lp.logPool[name]
	if !ok {
		l, err := log.New(lp.pool, name, lp.numLog, lp.chunkSize, createIsMissing)
		if err != nil {
			return nil, err
		}
		lpi = &logPoolItem{
			l: l,
		}
		lp.logPool[name] = lpi
	}
	lpi.lastAccess = time.Now()
	return lpi, nil
}

func (lp *LogPool) Get(name string) (*log.Log, error) {
	lp.m.Lock()
	defer lp.m.Unlock()

	lpi, err := lp.get(name, true)
	if err != nil {
		return nil, err
	}

	return lpi.l, nil
}

func (lp *LogPool) GetExist(name string) (*log.Log, error) {
	lp.m.Lock()
	defer lp.m.Unlock()

	lpi, err := lp.get(name, false)
	if err != nil {
		return nil, err
	}

	return lpi.l, nil
}

func (lp *LogPool) gc(closeNotify <-chan struct{}) {
	for {
		select {
		case <-time.After(5 * time.Minute):
			lp.m.Lock()
			n := time.Now()
			d15min := 60 * time.Minute
			for k, v := range lp.logPool {
				if n.Sub(v.lastAccess) > d15min && v.l.Listeners() == 0 {
					delete(lp.logPool, k)
				}
			}
			lp.m.Unlock()
		case <-closeNotify:
			return
		}
	}
}
