package pool

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"github.com/syndtr/goleveldb/leveldb"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"
)

func escape(s string) string {
	s = url.QueryEscape(s)
	s = strings.Replace(s, ".", "%2E", -1)
	s = strings.Replace(s, "~", "%7E", -1)
	s = strings.Replace(s, "%2F", "/", -1)
	return s
}

type Pool struct {
	baseDir     string
	pool        map[string]*poolItem
	closeNotify chan<- struct{}
	m           sync.Mutex
}

func NewPool(baseDir string) *Pool {
	ch := make(chan struct{}, 0)
	r := &Pool{
		baseDir:     baseDir,
		pool:        make(map[string]*poolItem),
		closeNotify: ch,
	}
	go r.gc(ch)
	return r
}

func (pool *Pool) Close() {
	pool.closeNotify <- struct{}{}
	pool.m.Lock()
	defer pool.m.Unlock()
	for k, v := range pool.pool {
		delete(pool.pool, k)
		v.db.Close()
	}
}

type poolItem struct {
	db         kvs.KVS
	lastAccess time.Time
}

func (pool *Pool) GetExist(name string) (kvs.KVS, error) {
	return pool.get(name, false)
}

func (pool *Pool) Get(name string) (kvs.KVS, error) {
	return pool.get(name, true)
}

func (pool *Pool) get(name string, createIfMissing bool) (kvs.KVS, error) {
	pool.m.Lock()
	defer pool.m.Unlock()

	p, ok := pool.pool[name]
	if !ok {
		dbpath := pool.baseDir + escape(name)
		if !createIfMissing {
			if _, err := os.Stat(dbpath); os.IsNotExist(err) {
				return nil, kvs.ErrNotExist
			}
		}
		db, err := leveldb.OpenFile(dbpath, nil)
		if err != nil {
			return nil, err
		}

		/*
			err = db.CompactRange(leveldb.Range{})
			if err != nil {
				return nil, err
			}
		*/

		p = &poolItem{
			db: kvs.KVS(NewLevelDBAdapter(db)),
		}
		pool.pool[name] = p
	}
	p.lastAccess = time.Now()
	return p.db, nil
}

func (pool *Pool) gc(closeNotify <-chan struct{}) {
	for {
		select {
		case <-time.After(60 * time.Second):
			pool.m.Lock()
			n := time.Now()
			d15min := 15 * time.Minute
			for k, v := range pool.pool {
				if n.Sub(v.lastAccess) > d15min {
					delete(pool.pool, k)
					v.db.Close()
				}
			}
			pool.m.Unlock()
		case <-closeNotify:
			return
		}
	}
}
