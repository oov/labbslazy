package pool

import (
	"bitbucket.org/oov/labbslazy/kvs"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
)

type LevelDBAdapter struct {
	db *leveldb.DB
}

type LevelDBAdapterBatch struct {
	db    *leveldb.DB
	batch *leveldb.Batch
}

func NewLevelDBAdapter(db *leveldb.DB) *LevelDBAdapter {
	return &LevelDBAdapter{
		db: db,
	}
}

func (l *LevelDBAdapter) Close() error {
	l.db.Close()
	return nil
}

func (l *LevelDBAdapter) Delete(key []byte) error {
	return l.db.Delete(key, &opt.WriteOptions{})
}

func (l *LevelDBAdapter) Get(key []byte) ([]byte, error) {
	r, err := l.db.Get(key, &opt.ReadOptions{})
	if err != nil && err.Error() == "not found" {
		err = kvs.ErrKVSKeyNotFound
	}
	return r, err
}

func (l *LevelDBAdapter) GetMsgpack(key []byte, value interface{}) error {
	return kvs.GetMsgpack(l, key, value)
}

func (l *LevelDBAdapter) Set(key, value []byte) error {
	return l.db.Put(key, value, &opt.WriteOptions{})
}

func (l *LevelDBAdapter) SetMsgpack(key []byte, value interface{}) error {
	return kvs.SetMsgpack(l, key, value)
}

func (l *LevelDBAdapter) NewBatch() kvs.Batch {
	return kvs.Batch(&LevelDBAdapterBatch{
		db:    l.db,
		batch: new(leveldb.Batch),
	})
}

func (b *LevelDBAdapterBatch) Delete(key []byte) error {
	b.batch.Delete(key)
	return nil
}

func (b *LevelDBAdapterBatch) Set(key, value []byte) error {
	b.batch.Put(key, value)
	return nil
}

func (b *LevelDBAdapterBatch) SetMsgpack(key []byte, value interface{}) error {
	return kvs.SetMsgpack(b, key, value)
}

func (b *LevelDBAdapterBatch) Write() error {
	return b.db.Write(b.batch, &opt.WriteOptions{})
}
